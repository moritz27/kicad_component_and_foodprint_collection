EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	7360 3010 7360 3110
Wire Wire Line
	6910 3010 7360 3010
$Comp
L my_libary:MP1584EN(SOIC8E) U?
U 1 1 61138617
P 5860 3110
F 0 "U?" H 5860 3497 60  0000 C CNN
F 1 "MP1584EN(SOIC8E)" H 5860 3391 60  0000 C CNN
F 2 "OLIMEX_Regulators-FP:SOIC8E" H 5860 2610 50  0001 C CNN
F 3 "https://www.monolithicpower.com/en/documentview/productdocument/index/version/2/document_type/Datasheet/lang/en/sku/MP1584/document_id/204" H 5860 2510 50  0001 C CNN
	1    5860 3110
	1    0    0    -1  
$EndComp
Wire Wire Line
	6610 3010 6260 3010
$Comp
L Device:C C?
U 1 1 5D5B5342
P 6760 3010
F 0 "C?" V 6508 3010 50  0000 C CNN
F 1 "100nF" V 6599 3010 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6798 2860 50  0001 C CNN
F 3 "~" H 6760 3010 50  0001 C CNN
	1    6760 3010
	0    1    1    0   
$EndComp
Connection ~ 7860 3210
Connection ~ 7860 3110
Wire Wire Line
	7860 3110 7860 3210
Wire Wire Line
	7760 3110 7860 3110
Connection ~ 7360 3110
Wire Wire Line
	7460 3110 7360 3110
Wire Wire Line
	7260 3210 7860 3210
Connection ~ 7360 4060
Wire Wire Line
	7360 4060 7860 4060
Wire Wire Line
	7860 3760 7860 4060
Wire Wire Line
	7860 3210 7860 3460
$Comp
L Device:C C?
U 1 1 6113861B
P 7860 3610
F 0 "C?" H 7975 3656 50  0000 L CNN
F 1 "22uF 6V3" H 7975 3565 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 7898 3460 50  0001 C CNN
F 3 "~" H 7860 3610 50  0001 C CNN
	1    7860 3610
	1    0    0    -1  
$EndComp
Wire Wire Line
	6760 3210 6260 3210
Connection ~ 6760 3210
Wire Wire Line
	6760 3460 6760 3210
Wire Wire Line
	6760 4060 7360 4060
Wire Wire Line
	7360 3760 7360 4060
Wire Wire Line
	7360 3460 7360 3110
$Comp
L Device:D_Schottky D?
U 1 1 5D5572F8
P 7360 3610
F 0 "D?" V 7260 3710 50  0000 L CNN
F 1 "SS34" V 7510 3660 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 7360 3610 50  0001 C CNN
F 3 "http://www.chinesechip.com/files/2015-06/8b5aa353-e559-4708-bf71-5dbd56a0a342.pdf" H 7360 3610 50  0001 C CNN
	1    7360 3610
	0    1    1    0   
$EndComp
Wire Wire Line
	6460 4010 6460 4060
Wire Wire Line
	6460 3660 6460 3710
Wire Wire Line
	4960 3310 4960 4060
Wire Wire Line
	5310 3860 5310 4060
Wire Wire Line
	4960 4060 5310 4060
Wire Wire Line
	6460 4210 6460 4060
Connection ~ 6760 4060
Connection ~ 6460 4060
Wire Wire Line
	6760 4060 6760 3760
Wire Wire Line
	6460 4060 6760 4060
Wire Wire Line
	6260 3110 7360 3110
Wire Wire Line
	6960 3210 6760 3210
$Comp
L Device:R R?
U 1 1 5D50F042
P 7110 3210
F 0 "R?" V 7210 3110 50  0000 C CNN
F 1 "210k" V 7210 3310 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7040 3210 50  0001 C CNN
F 3 "~" H 7110 3210 50  0001 C CNN
	1    7110 3210
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 6113861F
P 6760 3610
F 0 "R?" H 6830 3656 50  0000 L CNN
F 1 "40k2" H 6830 3565 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6690 3610 50  0001 C CNN
F 3 "~" H 6760 3610 50  0001 C CNN
	1    6760 3610
	1    0    0    -1  
$EndComp
Wire Wire Line
	6460 3310 6460 3360
Wire Wire Line
	6260 3310 6460 3310
$Comp
L Device:C C?
U 1 1 5D5142B3
P 6460 3510
F 0 "C?" H 6210 3560 50  0000 L CNN
F 1 "220pF" H 6110 3410 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6498 3360 50  0001 C CNN
F 3 "~" H 6460 3510 50  0001 C CNN
	1    6460 3510
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D513180
P 6460 3860
F 0 "R?" H 6210 3910 50  0000 L CNN
F 1 "68k1" H 6160 3810 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6390 3860 50  0001 C CNN
F 3 "~" H 6460 3860 50  0001 C CNN
	1    6460 3860
	1    0    0    -1  
$EndComp
$Comp
L Device:L L?
U 1 1 5D507F09
P 7610 3110
F 0 "L?" V 7429 3110 50  0000 C CNN
F 1 "10uH" V 7520 3110 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H3.5" H 7610 3110 50  0001 C CNN
F 3 "~" H 7610 3110 50  0001 C CNN
	1    7610 3110
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D4FB9C2
P 5310 3710
F 0 "R?" H 5380 3756 50  0000 L CNN
F 1 "100M" H 5380 3665 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5240 3710 50  0001 C CNN
F 3 "~" H 5310 3710 50  0001 C CNN
	1    5310 3710
	1    0    0    -1  
$EndComp
Wire Wire Line
	5310 3210 5460 3210
Wire Wire Line
	5310 3560 5310 3210
Wire Wire Line
	4960 3310 5460 3310
$Comp
L Device:C C?
U 1 1 5D4E75DF
P 4960 3160
F 0 "C?" H 5075 3206 50  0000 L CNN
F 1 "10uF" H 5075 3115 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4998 3010 50  0001 C CNN
F 3 "~" H 4960 3160 50  0001 C CNN
	1    4960 3160
	1    0    0    -1  
$EndComp
Text HLabel 4260 3010 0    50   Input ~ 0
Vin
Text HLabel 8460 3110 2    50   Output ~ 0
Vout_5V
Wire Wire Line
	8460 3110 7860 3110
Wire Wire Line
	5310 3110 5310 2760
Wire Wire Line
	5310 2760 4260 2760
Text HLabel 4260 2760 0    50   Input ~ 0
Enable
Wire Wire Line
	4260 3010 4960 3010
Connection ~ 4960 3010
Wire Wire Line
	4960 3010 5460 3010
Connection ~ 4960 3310
Wire Wire Line
	5460 3110 5310 3110
Connection ~ 5310 4060
Wire Wire Line
	5310 4060 6460 4060
$Comp
L power:GND #PWR?
U 1 1 5DF25201
P 6460 4210
F 0 "#PWR?" H 6460 3960 50  0001 C CNN
F 1 "GND" H 6465 4037 50  0000 C CNN
F 2 "" H 6460 4210 50  0001 C CNN
F 3 "" H 6460 4210 50  0001 C CNN
	1    6460 4210
	1    0    0    -1  
$EndComp
$EndSCHEMATC
