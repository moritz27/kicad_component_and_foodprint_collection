EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 12 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5380 3400 5410 3400
Wire Wire Line
	5380 3600 5420 3600
Wire Wire Line
	6030 3400 6060 3400
Wire Wire Line
	6110 3600 6110 3900
Wire Wire Line
	6030 3600 6110 3600
$Comp
L power:GND #PWR?
U 1 1 60F63136
P 6110 4040
AR Path="/5FDB8EF8/60F63136" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/60F63136" Ref="#PWR?"  Part="1" 
AR Path="/60F63136" Ref="#PWR?"  Part="1" 
AR Path="/60F609D8/60F63136" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6110 3790 50  0001 C CNN
F 1 "GND" H 6115 3867 50  0000 C CNN
F 2 "" H 6110 4040 50  0001 C CNN
F 3 "" H 6110 4040 50  0001 C CNN
	1    6110 4040
	1    0    0    -1  
$EndComp
Wire Wire Line
	4790 3500 5430 3500
Wire Wire Line
	4790 3580 4790 3500
$Comp
L power:GND #PWR?
U 1 1 60F6313F
P 4790 3580
AR Path="/5FDB8EF8/60F6313F" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/60F6313F" Ref="#PWR?"  Part="1" 
AR Path="/60F6313F" Ref="#PWR?"  Part="1" 
AR Path="/60F609D8/60F6313F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4790 3330 50  0001 C CNN
F 1 "GND" H 4795 3407 50  0000 C CNN
F 2 "" H 4790 3580 50  0001 C CNN
F 3 "" H 4790 3580 50  0001 C CNN
	1    4790 3580
	1    0    0    -1  
$EndComp
$Comp
L Device:Rotary_Encoder_Switch SW?
U 1 1 60F63147
P 5730 3500
AR Path="/5FDB8EF8/60F63147" Ref="SW?"  Part="1" 
AR Path="/6003A36D/60F63147" Ref="SW?"  Part="1" 
AR Path="/60F609D8/60F63147" Ref="SW?"  Part="1" 
F 0 "SW?" H 5730 3770 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 5730 3776 50  0001 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC11E-Switch_Vertical_H20mm" H 5580 3660 50  0001 C CNN
F 3 "~" H 5730 3760 50  0001 C CNN
	1    5730 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60F6314D
P 5180 3250
AR Path="/5FDB8EF8/60F6314D" Ref="R?"  Part="1" 
AR Path="/6003A36D/60F6314D" Ref="R?"  Part="1" 
AR Path="/60F609D8/60F6314D" Ref="R?"  Part="1" 
F 0 "R?" V 5090 3180 50  0000 C CNN
F 1 "10k" V 5090 3320 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5110 3250 50  0001 C CNN
F 3 "~" H 5180 3250 50  0001 C CNN
	1    5180 3250
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 60F63153
P 5180 3900
AR Path="/5FDB8EF8/60F63153" Ref="R?"  Part="1" 
AR Path="/6003A36D/60F63153" Ref="R?"  Part="1" 
AR Path="/60F609D8/60F63153" Ref="R?"  Part="1" 
F 0 "R?" V 5090 3820 50  0000 C CNN
F 1 "10k" V 5090 3970 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5110 3900 50  0001 C CNN
F 3 "~" H 5180 3900 50  0001 C CNN
	1    5180 3900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5030 3250 4500 3250
Wire Wire Line
	5030 3900 4500 3900
Wire Wire Line
	4500 3900 4500 3250
Wire Wire Line
	5330 3900 5420 3900
Wire Wire Line
	5420 3900 5420 3600
Connection ~ 5420 3600
Wire Wire Line
	5420 3600 5430 3600
Wire Wire Line
	5330 3250 5410 3250
Wire Wire Line
	5410 3250 5410 3400
Connection ~ 5410 3400
Wire Wire Line
	5410 3400 5430 3400
$Comp
L Device:C C?
U 1 1 60F6316C
P 5740 3900
AR Path="/5FDB8EF8/60F6316C" Ref="C?"  Part="1" 
AR Path="/6003A36D/60F6316C" Ref="C?"  Part="1" 
AR Path="/60F609D8/60F6316C" Ref="C?"  Part="1" 
F 0 "C?" V 5870 3950 50  0000 L CNN
F 1 "100nF" V 5870 3660 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5778 3750 50  0001 C CNN
F 3 "~" H 5740 3900 50  0001 C CNN
	1    5740 3900
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 60F63172
P 5640 3080
AR Path="/5FDB8EF8/60F63172" Ref="C?"  Part="1" 
AR Path="/6003A36D/60F63172" Ref="C?"  Part="1" 
AR Path="/60F609D8/60F63172" Ref="C?"  Part="1" 
F 0 "C?" V 5770 3130 50  0000 L CNN
F 1 "100nF" V 5770 2840 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5678 2930 50  0001 C CNN
F 3 "~" H 5640 3080 50  0001 C CNN
	1    5640 3080
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5410 3250 5410 3080
Wire Wire Line
	5410 3080 5490 3080
Connection ~ 5410 3250
Wire Wire Line
	5420 3900 5590 3900
Connection ~ 5420 3900
Wire Wire Line
	5890 3900 6110 3900
Connection ~ 6110 3900
Wire Wire Line
	6110 3900 6110 4040
$Comp
L Device:R R?
U 1 1 60F63180
P 6360 3070
AR Path="/5FDB8EF8/60F63180" Ref="R?"  Part="1" 
AR Path="/6003A36D/60F63180" Ref="R?"  Part="1" 
AR Path="/60F609D8/60F63180" Ref="R?"  Part="1" 
F 0 "R?" V 6270 2980 50  0000 C CNN
F 1 "10k" V 6270 3140 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6290 3070 50  0001 C CNN
F 3 "~" H 6360 3070 50  0001 C CNN
	1    6360 3070
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6060 3400 6060 3070
Wire Wire Line
	6060 3070 6210 3070
Connection ~ 6060 3400
Wire Wire Line
	6060 3400 6140 3400
$Comp
L power:GND #PWR?
U 1 1 60F6318A
P 5920 2880
AR Path="/5FDB8EF8/60F6318A" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/60F6318A" Ref="#PWR?"  Part="1" 
AR Path="/60F6318A" Ref="#PWR?"  Part="1" 
AR Path="/60F609D8/60F6318A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5920 2630 50  0001 C CNN
F 1 "GND" H 5925 2707 50  0000 C CNN
F 2 "" H 5920 2880 50  0001 C CNN
F 3 "" H 5920 2880 50  0001 C CNN
	1    5920 2880
	-1   0    0    1   
$EndComp
Wire Wire Line
	5790 3080 5920 3080
Wire Wire Line
	5920 3080 5920 2880
$Comp
L Device:C C?
U 1 1 60F6319A
P 6360 3720
AR Path="/5FDB8EF8/60F6319A" Ref="C?"  Part="1" 
AR Path="/6003A36D/60F6319A" Ref="C?"  Part="1" 
AR Path="/60F609D8/60F6319A" Ref="C?"  Part="1" 
F 0 "C?" H 6480 3760 50  0000 L CNN
F 1 "100nF" H 6470 3670 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6398 3570 50  0001 C CNN
F 3 "~" H 6360 3720 50  0001 C CNN
	1    6360 3720
	1    0    0    -1  
$EndComp
Wire Wire Line
	6060 3400 6060 3520
Wire Wire Line
	6060 3520 6360 3520
Wire Wire Line
	6360 3520 6360 3570
Wire Wire Line
	6110 3900 6360 3900
Wire Wire Line
	6360 3900 6360 3870
Text HLabel 5380 3400 0    50   Output ~ 0
Encoder_A
Text HLabel 5380 3600 0    50   Output ~ 0
Encoder_B
Text HLabel 6140 3400 2    50   Output ~ 0
Encoder_Button
Text HLabel 4400 3250 0    50   Input ~ 0
Vcc
Text HLabel 6660 3070 2    50   Input ~ 0
Vcc
Wire Wire Line
	6510 3070 6660 3070
Wire Wire Line
	4400 3250 4500 3250
Connection ~ 4500 3250
Wire Wire Line
	8560 3790 8730 3790
Wire Wire Line
	8560 3860 8560 3790
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 60FA8625
P 8930 3590
AR Path="/5FDB8EF8/60FA8625" Ref="J?"  Part="1" 
AR Path="/6003A36D/60FA8625" Ref="J?"  Part="1" 
AR Path="/60EDB96D/60FA8625" Ref="J?"  Part="1" 
AR Path="/60F609D8/60FA8625" Ref="J?"  Part="1" 
F 0 "J?" H 9010 3582 50  0000 L CNN
F 1 "Conn_01x04" H 9010 3491 50  0001 L CNN
F 2 "Connector_JST:JST_PH_B4B-PH-K_1x04_P2.00mm_Vertical" H 8930 3590 50  0001 C CNN
F 3 "~" H 8930 3590 50  0001 C CNN
	1    8930 3590
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60FA862B
P 8560 3860
AR Path="/5FDB8EF8/60FA862B" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/60FA862B" Ref="#PWR?"  Part="1" 
AR Path="/60FA862B" Ref="#PWR?"  Part="1" 
AR Path="/60EDB96D/60FA862B" Ref="#PWR?"  Part="1" 
AR Path="/60F609D8/60FA862B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8560 3610 50  0001 C CNN
F 1 "GND" H 8565 3687 50  0000 C CNN
F 2 "" H 8560 3860 50  0001 C CNN
F 3 "" H 8560 3860 50  0001 C CNN
	1    8560 3860
	1    0    0    -1  
$EndComp
Text HLabel 8500 3490 0    50   Output ~ 0
Encoder_A
Text HLabel 8500 3590 0    50   Output ~ 0
Encoder_B
Text HLabel 8500 3690 0    50   Output ~ 0
Encoder_Button
Wire Wire Line
	8500 3490 8730 3490
Wire Wire Line
	8500 3590 8730 3590
Wire Wire Line
	8500 3690 8730 3690
$EndSCHEMATC
