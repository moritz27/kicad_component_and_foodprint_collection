EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 16 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5585 4485 5585 4600
Wire Wire Line
	5585 4600 5785 4600
Wire Wire Line
	5785 4600 5785 4485
$Comp
L power:GND #PWR?
U 1 1 60C93397
P 5785 4920
F 0 "#PWR?" H 5785 4670 50  0001 C CNN
F 1 "GND" H 5790 4747 50  0000 C CNN
F 2 "" H 5785 4920 50  0001 C CNN
F 3 "" H 5785 4920 50  0001 C CNN
	1    5785 4920
	1    0    0    -1  
$EndComp
Wire Wire Line
	5785 4920 5785 4875
Connection ~ 5785 4600
$Comp
L Device:R R?
U 1 1 60E6829E
P 5135 4535
AR Path="/60C9915C/60E6829E" Ref="R?"  Part="1" 
AR Path="/60E6829E" Ref="R?"  Part="1" 
AR Path="/61138355/60E6829E" Ref="R?"  Part="1" 
F 0 "R?" V 5045 4455 50  0000 C CNN
F 1 "10k" V 5045 4625 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5065 4535 50  0001 C CNN
F 3 "~" H 5135 4535 50  0001 C CNN
	1    5135 4535
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 60E6A002
P 4940 4530
AR Path="/60C9915C/60E6A002" Ref="R?"  Part="1" 
AR Path="/60E6A002" Ref="R?"  Part="1" 
AR Path="/61138355/60E6A002" Ref="R?"  Part="1" 
F 0 "R?" V 4850 4450 50  0000 C CNN
F 1 "10k" V 4850 4620 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4870 4530 50  0001 C CNN
F 3 "~" H 4940 4530 50  0001 C CNN
	1    4940 4530
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 60E6A28B
P 4735 4525
AR Path="/60C9915C/60E6A28B" Ref="R?"  Part="1" 
AR Path="/60E6A28B" Ref="R?"  Part="1" 
AR Path="/61138355/60E6A28B" Ref="R?"  Part="1" 
F 0 "R?" V 4645 4445 50  0000 C CNN
F 1 "10k" V 4645 4615 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4665 4525 50  0001 C CNN
F 3 "~" H 4735 4525 50  0001 C CNN
	1    4735 4525
	-1   0    0    1   
$EndComp
Wire Wire Line
	4735 4375 4735 3585
Wire Wire Line
	4940 4380 4940 3685
Wire Wire Line
	5135 4385 5135 3785
Wire Wire Line
	4735 4675 4735 4875
Connection ~ 5785 4875
Wire Wire Line
	5785 4875 5785 4600
Wire Wire Line
	4940 4680 4940 4875
Wire Wire Line
	5135 4685 5135 4875
Wire Wire Line
	5185 3385 4545 3385
Wire Wire Line
	5185 3285 4340 3285
Connection ~ 4735 3585
Wire Wire Line
	4735 3585 5185 3585
Connection ~ 4940 3685
Wire Wire Line
	4940 3685 5185 3685
Connection ~ 5135 3785
Wire Wire Line
	5135 3785 5185 3785
Connection ~ 5135 4875
Wire Wire Line
	5135 4875 5785 4875
Wire Wire Line
	4270 3585 4735 3585
Wire Wire Line
	4270 3685 4940 3685
Wire Wire Line
	4270 3785 5135 3785
$Comp
L Device:R R?
U 1 1 60EB6EE1
P 4545 4525
AR Path="/60C9915C/60EB6EE1" Ref="R?"  Part="1" 
AR Path="/60EB6EE1" Ref="R?"  Part="1" 
AR Path="/61138355/60EB6EE1" Ref="R?"  Part="1" 
F 0 "R?" V 4455 4445 50  0000 C CNN
F 1 "10k" V 4455 4615 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4475 4525 50  0001 C CNN
F 3 "~" H 4545 4525 50  0001 C CNN
	1    4545 4525
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 60EB6EE7
P 4340 4520
AR Path="/60C9915C/60EB6EE7" Ref="R?"  Part="1" 
AR Path="/60EB6EE7" Ref="R?"  Part="1" 
AR Path="/61138355/60EB6EE7" Ref="R?"  Part="1" 
F 0 "R?" V 4250 4440 50  0000 C CNN
F 1 "10k" V 4250 4610 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4270 4520 50  0001 C CNN
F 3 "~" H 4340 4520 50  0001 C CNN
	1    4340 4520
	-1   0    0    1   
$EndComp
Wire Wire Line
	4545 4675 4545 4875
Wire Wire Line
	4545 4875 4735 4875
Wire Wire Line
	4545 4875 4340 4875
Wire Wire Line
	4340 4875 4340 4670
Connection ~ 4545 4875
Wire Wire Line
	4545 4375 4545 3385
Connection ~ 4545 3385
Wire Wire Line
	4545 3385 4275 3385
Wire Wire Line
	4340 4370 4340 3285
Connection ~ 4340 3285
Connection ~ 4735 4875
Wire Wire Line
	4735 4875 4940 4875
Connection ~ 4940 4875
Wire Wire Line
	4940 4875 5135 4875
Wire Wire Line
	5185 4185 4445 4185
Wire Wire Line
	5185 4085 4650 4085
Wire Wire Line
	5185 3985 4840 3985
$Comp
L Device:R R?
U 1 1 60F7F7C0
P 4840 3025
AR Path="/60C9915C/60F7F7C0" Ref="R?"  Part="1" 
AR Path="/60F7F7C0" Ref="R?"  Part="1" 
AR Path="/61138355/60F7F7C0" Ref="R?"  Part="1" 
F 0 "R?" V 4750 2945 50  0000 C CNN
F 1 "10k" V 4750 3115 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4770 3025 50  0001 C CNN
F 3 "~" H 4840 3025 50  0001 C CNN
	1    4840 3025
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 60F7F7C6
P 4650 3025
AR Path="/60C9915C/60F7F7C6" Ref="R?"  Part="1" 
AR Path="/60F7F7C6" Ref="R?"  Part="1" 
AR Path="/61138355/60F7F7C6" Ref="R?"  Part="1" 
F 0 "R?" V 4560 2945 50  0000 C CNN
F 1 "10k" V 4560 3115 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4580 3025 50  0001 C CNN
F 3 "~" H 4650 3025 50  0001 C CNN
	1    4650 3025
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 60F7F7CC
P 4445 3020
AR Path="/60C9915C/60F7F7CC" Ref="R?"  Part="1" 
AR Path="/60F7F7CC" Ref="R?"  Part="1" 
AR Path="/61138355/60F7F7CC" Ref="R?"  Part="1" 
F 0 "R?" V 4355 2940 50  0000 C CNN
F 1 "10k" V 4355 3110 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4375 3020 50  0001 C CNN
F 3 "~" H 4445 3020 50  0001 C CNN
	1    4445 3020
	-1   0    0    1   
$EndComp
Wire Wire Line
	4445 3170 4445 4185
Wire Wire Line
	4650 4085 4650 3175
Wire Wire Line
	4840 3175 4840 3985
Wire Wire Line
	4445 2805 4650 2805
Wire Wire Line
	4650 2805 4650 2875
Connection ~ 4445 2805
Wire Wire Line
	4445 2805 4445 2870
Wire Wire Line
	4650 2805 4840 2805
Wire Wire Line
	4840 2805 4840 2875
Connection ~ 4650 2805
Wire Wire Line
	6085 3585 6370 3585
Wire Wire Line
	6370 3685 6085 3685
Wire Wire Line
	6085 3785 6370 3785
Wire Wire Line
	6370 3885 6085 3885
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 60C90844
P 6570 3785
F 0 "J?" H 6650 3777 50  0000 L CNN
F 1 "Conn_01x04" H 6650 3686 50  0001 L CNN
F 2 "Connector_JST:JST_XH_B4B-XH-A_1x04_P2.50mm_Vertical" H 6570 3785 50  0001 C CNN
F 3 "~" H 6570 3785 50  0001 C CNN
	1    6570 3785
	1    0    0    1   
$EndComp
$Comp
L Device:CP C?
U 1 1 610F293A
P 6320 3010
AR Path="/60C9915C/610F293A" Ref="C?"  Part="1" 
AR Path="/610F293A" Ref="C?"  Part="1" 
AR Path="/61138355/610F293A" Ref="C?"  Part="1" 
F 0 "C?" H 6435 3056 50  0000 L CNN
F 1 "1000uF" H 6435 2965 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_8x10.5" H 6358 2860 50  0001 C CNN
F 3 "~" H 6320 3010 50  0001 C CNN
	1    6320 3010
	1    0    0    -1  
$EndComp
Wire Wire Line
	6320 2860 6320 2830
Wire Wire Line
	6320 2830 5785 2830
Connection ~ 5785 2830
Wire Wire Line
	5785 2830 5785 2985
$Comp
L power:GND #PWR?
U 1 1 610F9CC7
P 6320 3195
F 0 "#PWR?" H 6320 2945 50  0001 C CNN
F 1 "GND" H 6325 3022 50  0000 C CNN
F 2 "" H 6320 3195 50  0001 C CNN
F 3 "" H 6320 3195 50  0001 C CNN
	1    6320 3195
	1    0    0    -1  
$EndComp
Wire Wire Line
	6320 3160 6320 3195
$Comp
L Device:C C?
U 1 1 61188E56
P 5290 2630
AR Path="/60C9915C/61188E56" Ref="C?"  Part="1" 
AR Path="/61188E56" Ref="C?"  Part="1" 
AR Path="/61138355/61188E56" Ref="C?"  Part="1" 
F 0 "C?" H 5405 2676 50  0000 L CNN
F 1 "100nF" H 5405 2585 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5328 2480 50  0001 C CNN
F 3 "~" H 5290 2630 50  0001 C CNN
	1    5290 2630
	-1   0    0    1   
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 A?
U 1 1 5F77148A
P 5585 3685
F 0 "A?" H 5940 4380 50  0000 C CNN
F 1 "Pololu_Breakout_A4988" H 5635 4475 50  0001 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 5860 2935 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 5685 3385 50  0001 C CNN
	1    5585 3685
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 611A2503
P 5290 2815
F 0 "#PWR?" H 5290 2565 50  0001 C CNN
F 1 "GND" H 5295 2642 50  0000 C CNN
F 2 "" H 5290 2815 50  0001 C CNN
F 3 "" H 5290 2815 50  0001 C CNN
	1    5290 2815
	1    0    0    -1  
$EndComp
Wire Wire Line
	5290 2480 5290 2445
Wire Wire Line
	5290 2445 5585 2445
Connection ~ 5585 2445
Wire Wire Line
	5585 2445 5585 2985
Wire Wire Line
	5290 2780 5290 2815
Text HLabel 4275 3285 0    50   Input ~ 0
Reset
Text HLabel 4275 3385 0    50   Input ~ 0
Sleep
Wire Wire Line
	4275 3285 4340 3285
Text HLabel 4270 3585 0    50   Input ~ 0
Enable
Text HLabel 4270 3685 0    50   Input ~ 0
Step
Text HLabel 4270 3785 0    50   Input ~ 0
Dir
Text HLabel 5785 2570 1    50   Input ~ 0
Vmot
Wire Wire Line
	5785 2570 5785 2830
Text HLabel 4305 2155 0    50   Input ~ 0
Vdd
Wire Wire Line
	4445 2155 4305 2155
Wire Wire Line
	4445 2155 4445 2805
Wire Wire Line
	4445 2155 5585 2155
Wire Wire Line
	5585 2155 5585 2445
Connection ~ 4445 2155
$EndSCHEMATC
