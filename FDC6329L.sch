EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 19 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L my_libary:FDC6329L U?
U 1 1 60215CBE
P 6550 3640
F 0 "U?" H 7120 4030 50  0000 C CNN
F 1 "FDC6329L" H 6820 4030 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 7000 3540 50  0001 C CNN
F 3 "" H 6550 3640 50  0001 C CNN
	1    6550 3640
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6021E9AB
P 4430 3890
AR Path="/5FD94276/6021E9AB" Ref="C?"  Part="1" 
AR Path="/5FD932E8/6021E9AB" Ref="C?"  Part="1" 
AR Path="/6021E9AB" Ref="C?"  Part="1" 
AR Path="/611718CE/6021E9AB" Ref="C?"  Part="1" 
F 0 "C?" H 4560 3850 50  0000 L CNN
F 1 "100nF" H 4560 3960 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4468 3740 50  0001 C CNN
F 3 "~" H 4430 3890 50  0001 C CNN
	1    4430 3890
	1    0    0    1   
$EndComp
Connection ~ 4430 3590
Wire Wire Line
	4430 3590 4430 3740
Wire Wire Line
	5750 3590 4430 3590
$Comp
L power:GND #PWR?
U 1 1 602349EC
P 4430 4110
F 0 "#PWR?" H 4430 3860 50  0001 C CNN
F 1 "GND" H 4435 3937 50  0000 C CNN
F 2 "" H 4430 4110 50  0001 C CNN
F 3 "" H 4430 4110 50  0001 C CNN
	1    4430 4110
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4430 4110 4430 4040
Wire Wire Line
	6650 3490 6810 3490
Wire Wire Line
	6650 3590 6810 3590
Wire Wire Line
	6810 3590 6810 3490
Connection ~ 6810 3490
Wire Wire Line
	6960 3880 6960 3950
$Comp
L power:GND #PWR?
U 1 1 6028C9D7
P 6960 4000
F 0 "#PWR?" H 6960 3750 50  0001 C CNN
F 1 "GND" H 6965 3827 50  0000 C CNN
F 2 "" H 6960 4000 50  0001 C CNN
F 3 "" H 6960 4000 50  0001 C CNN
	1    6960 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6029C055
P 7420 3740
AR Path="/5FD94276/6029C055" Ref="C?"  Part="1" 
AR Path="/5FD932E8/6029C055" Ref="C?"  Part="1" 
AR Path="/6029C055" Ref="C?"  Part="1" 
AR Path="/611718CE/6029C055" Ref="C?"  Part="1" 
F 0 "C?" H 7190 3700 50  0000 L CNN
F 1 "1nF" H 7160 3790 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7458 3590 50  0001 C CNN
F 3 "~" H 7420 3740 50  0001 C CNN
	1    7420 3740
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 602A0A57
P 7890 3740
AR Path="/5FD94276/602A0A57" Ref="C?"  Part="1" 
AR Path="/5FD932E8/602A0A57" Ref="C?"  Part="1" 
AR Path="/602A0A57" Ref="C?"  Part="1" 
AR Path="/611718CE/602A0A57" Ref="C?"  Part="1" 
F 0 "C?" H 7660 3700 50  0000 L CNN
F 1 "10nF" H 7580 3780 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7928 3590 50  0001 C CNN
F 3 "~" H 7890 3740 50  0001 C CNN
	1    7890 3740
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6960 3580 6960 3490
Wire Wire Line
	6810 3490 6960 3490
Connection ~ 6960 3490
Wire Wire Line
	6960 3490 7420 3490
Wire Wire Line
	7420 3590 7420 3490
Connection ~ 7420 3490
Wire Wire Line
	7420 3490 7890 3490
Wire Wire Line
	7890 3590 7890 3490
Connection ~ 7890 3490
Wire Wire Line
	7890 3490 8180 3490
Wire Wire Line
	6960 3950 7420 3950
Wire Wire Line
	7420 3950 7420 3890
Connection ~ 6960 3950
Wire Wire Line
	6960 3950 6960 4000
Wire Wire Line
	7420 3950 7890 3950
Wire Wire Line
	7890 3950 7890 3890
Connection ~ 7420 3950
$Comp
L Device:C C?
U 1 1 6047854C
P 6170 3060
AR Path="/5FD94276/6047854C" Ref="C?"  Part="1" 
AR Path="/5FD932E8/6047854C" Ref="C?"  Part="1" 
AR Path="/6047854C" Ref="C?"  Part="1" 
AR Path="/611718CE/6047854C" Ref="C?"  Part="1" 
F 0 "C?" V 6320 2920 50  0000 L CNN
F 1 "100pF" V 6320 3060 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6208 2910 50  0001 C CNN
F 3 "~" H 6170 3060 50  0001 C CNN
	1    6170 3060
	0    1    -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 6047AF9A
P 6880 3390
AR Path="/5FD94276/6047AF9A" Ref="R?"  Part="1" 
AR Path="/6047AF9A" Ref="R?"  Part="1" 
AR Path="/611718CE/6047AF9A" Ref="R?"  Part="1" 
F 0 "R?" V 6960 3430 50  0000 L CNN
F 1 "2k2" V 6960 3260 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6810 3390 50  0001 C CNN
F 3 "~" H 6880 3390 50  0001 C CNN
	1    6880 3390
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6047B8BA
P 7090 3390
F 0 "#PWR?" H 7090 3140 50  0001 C CNN
F 1 "GND" H 7095 3217 50  0000 C CNN
F 2 "" H 7090 3390 50  0001 C CNN
F 3 "" H 7090 3390 50  0001 C CNN
	1    7090 3390
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7090 3390 7030 3390
Wire Wire Line
	6730 3390 6650 3390
Wire Wire Line
	6320 3060 7420 3060
Wire Wire Line
	7420 3060 7420 3490
Wire Wire Line
	5750 3390 5700 3390
Wire Wire Line
	5700 3060 6020 3060
Wire Wire Line
	5700 3060 5700 3390
$Comp
L Device:R R?
U 1 1 6048BF15
P 5480 3060
AR Path="/5FD94276/6048BF15" Ref="R?"  Part="1" 
AR Path="/6048BF15" Ref="R?"  Part="1" 
AR Path="/611718CE/6048BF15" Ref="R?"  Part="1" 
F 0 "R?" V 5570 3120 50  0000 L CNN
F 1 "100k" V 5570 2900 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5410 3060 50  0001 C CNN
F 3 "~" H 5480 3060 50  0001 C CNN
	1    5480 3060
	0    1    -1   0   
$EndComp
Wire Wire Line
	5630 3060 5700 3060
Connection ~ 5700 3060
Wire Wire Line
	5330 3060 4430 3060
Wire Wire Line
	4430 3060 4430 3590
Wire Wire Line
	5470 3490 5750 3490
Text HLabel 5470 3490 0    50   Input ~ 0
On
Text HLabel 4400 3060 0    50   Input ~ 0
Vin
Text HLabel 8180 3490 2    50   Output ~ 0
Vout
Wire Wire Line
	4400 3060 4430 3060
Connection ~ 4430 3060
$Comp
L Device:C C?
U 1 1 60287B88
P 6960 3730
AR Path="/5FD94276/60287B88" Ref="C?"  Part="1" 
AR Path="/5FD932E8/60287B88" Ref="C?"  Part="1" 
AR Path="/60287B88" Ref="C?"  Part="1" 
AR Path="/611718CE/60287B88" Ref="C?"  Part="1" 
F 0 "C?" H 6730 3690 50  0000 L CNN
F 1 "100pF" H 6590 3780 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6998 3580 50  0001 C CNN
F 3 "~" H 6960 3730 50  0001 C CNN
	1    6960 3730
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
