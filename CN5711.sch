EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 17 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L my_libary:CN5711 U?
U 1 1 5EEB9B1D
P 5600 3650
F 0 "U?" H 5550 4025 50  0000 C CNN
F 1 "CN5711" H 5550 3934 50  0000 C CNN
F 2 "Package_SO:HSOP-8-1EP_3.9x4.9mm_P1.27mm_EP2.41x3.1mm_ThermalVias" H 5600 3650 50  0001 C CNN
F 3 "" H 5600 3650 50  0001 C CNN
	1    5600 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EEBCBB3
P 4950 3500
F 0 "R?" V 4743 3500 50  0000 C CNN
F 1 "5k2" V 4834 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4880 3500 50  0001 C CNN
F 3 "~" H 4950 3500 50  0001 C CNN
	1    4950 3500
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EEBDCBF
P 5000 3700
F 0 "#PWR?" H 5000 3550 50  0001 C CNN
F 1 "+5V" V 5015 3828 50  0000 L CNN
F 2 "" H 5000 3700 50  0001 C CNN
F 3 "" H 5000 3700 50  0001 C CNN
	1    5000 3700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6200 3800 5850 3800
Wire Wire Line
	6200 3700 6200 3800
Connection ~ 6200 3800
Wire Wire Line
	5000 3700 5150 3700
Wire Wire Line
	5150 3700 5150 3800
Wire Wire Line
	5150 3800 5250 3800
Connection ~ 5150 3700
Wire Wire Line
	5150 3700 5250 3700
Wire Wire Line
	5100 3500 5250 3500
Wire Wire Line
	4450 3500 4700 3500
Wire Wire Line
	5250 3600 4700 3600
Wire Wire Line
	4700 3600 4700 3500
Connection ~ 4700 3500
Wire Wire Line
	4700 3500 4800 3500
Wire Wire Line
	5850 3500 6080 3500
$Comp
L power:GND #PWR?
U 1 1 5EF146E2
P 4450 3600
F 0 "#PWR?" H 4450 3350 50  0001 C CNN
F 1 "GND" H 4455 3427 50  0000 C CNN
F 2 "" H 4450 3600 50  0001 C CNN
F 3 "" H 4450 3600 50  0001 C CNN
	1    4450 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 4250 6450 4250
$Comp
L Device:LED_PAD D?
U 1 1 5EEB635A
P 6200 4250
F 0 "D?" V 6250 4550 50  0000 R CNN
F 1 "LED" V 6150 4600 50  0000 R CNN
F 2 "LED_SMD:LED_1W_3W_R8" H 6200 4250 50  0001 C CNN
F 3 "~" H 6200 4250 50  0001 C CNN
	1    6200 4250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6200 3800 6200 4100
NoConn ~ 6450 4250
NoConn ~ 7100 4250
$Comp
L Device:LED_PAD D?
U 1 1 5EEB8C28
P 6850 4250
F 0 "D?" V 6900 4500 50  0000 R CNN
F 1 "LED" V 6800 4500 50  0000 R CNN
F 2 "LED_SMD:LED_1W_3W_R8" H 6850 4250 50  0001 C CNN
F 3 "~" H 6850 4250 50  0001 C CNN
	1    6850 4250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7100 4250 7050 4250
Wire Wire Line
	6850 3800 6850 4100
$Comp
L power:GND #PWR?
U 1 1 5EEC847C
P 5950 4750
F 0 "#PWR?" H 5950 4500 50  0001 C CNN
F 1 "GND" H 5955 4577 50  0000 C CNN
F 2 "" H 5950 4750 50  0001 C CNN
F 3 "" H 5950 4750 50  0001 C CNN
	1    5950 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 4550 5950 4700
$Comp
L Jumper:SolderJumper_3_Open JP?
U 1 1 5EFB3DAD
P 6200 4550
F 0 "JP?" H 6200 4663 50  0000 C CNN
F 1 "Jumper" H 6200 4664 50  0001 C CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6200 4550 50  0001 C CNN
F 3 "~" H 6200 4550 50  0001 C CNN
	1    6200 4550
	-1   0    0    1   
$EndComp
Wire Wire Line
	5950 4550 6000 4550
Wire Wire Line
	6400 4550 6550 4550
Wire Wire Line
	6550 4550 6550 3800
Wire Wire Line
	6550 3800 6850 3800
Connection ~ 6550 3800
$Comp
L Jumper:SolderJumper_2_Open JP?
U 1 1 5EFBEE23
P 6350 3800
F 0 "JP?" H 6350 3913 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 6350 3914 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 6350 3800 50  0001 C CNN
F 3 "~" H 6350 3800 50  0001 C CNN
	1    6350 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 3800 6550 3800
Wire Wire Line
	5950 4700 6850 4700
Wire Wire Line
	6850 4400 6850 4700
Connection ~ 5950 4700
Wire Wire Line
	5950 4700 5950 4750
Wire Wire Line
	5850 3700 6200 3700
Wire Wire Line
	4450 3600 4450 3500
Text HLabel 6080 3500 2    50   Input ~ 0
LED_ON
$EndSCHEMATC
