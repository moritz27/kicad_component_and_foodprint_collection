EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 18 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Linear:LM1117-3.3 U?
U 1 1 61156A6F
P 5800 3350
F 0 "U?" H 5800 3592 50  0000 C CNN
F 1 "LM1117-3.3" H 5800 3501 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 5800 3350 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm1117.pdf" H 5800 3350 50  0001 C CNN
	1    5800 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61156A75
P 5800 3750
F 0 "#PWR?" H 5800 3500 50  0001 C CNN
F 1 "GND" H 5805 3577 50  0000 C CNN
F 2 "" H 5800 3750 50  0001 C CNN
F 3 "" H 5800 3750 50  0001 C CNN
	1    5800 3750
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 61156A7B
P 6450 3250
F 0 "#PWR?" H 6450 3100 50  0001 C CNN
F 1 "+3.3V" H 6450 3450 50  0000 C CNN
F 2 "" H 6450 3250 50  0001 C CNN
F 3 "" H 6450 3250 50  0001 C CNN
	1    6450 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3350 5500 3350
Wire Wire Line
	5800 3650 5800 3750
Wire Wire Line
	6100 3350 6150 3350
Wire Wire Line
	6450 3350 6450 3250
$Comp
L Device:C C?
U 1 1 61156A86
P 5200 3600
F 0 "C?" H 5250 3700 50  0000 L CNN
F 1 "10uF" H 5200 3500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5238 3450 50  0001 C CNN
F 3 "~" H 5200 3600 50  0001 C CNN
	1    5200 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 61156A8C
P 6450 3600
F 0 "C?" H 6500 3700 50  0000 L CNN
F 1 "10uF" H 6450 3500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6488 3450 50  0001 C CNN
F 3 "~" H 6450 3600 50  0001 C CNN
	1    6450 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 61156A92
P 6150 3600
F 0 "C?" H 6200 3700 50  0000 L CNN
F 1 "100nF" H 6150 3500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6188 3450 50  0001 C CNN
F 3 "~" H 6150 3600 50  0001 C CNN
	1    6150 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3750 6150 3750
Connection ~ 5800 3750
Wire Wire Line
	6150 3750 6450 3750
Connection ~ 6150 3750
Wire Wire Line
	6150 3450 6150 3350
Connection ~ 6150 3350
Wire Wire Line
	6150 3350 6450 3350
Wire Wire Line
	6450 3450 6450 3350
Connection ~ 6450 3350
Wire Wire Line
	5200 3450 5200 3350
Wire Wire Line
	5200 3750 5800 3750
Text HLabel 5075 3350 0    50   Input ~ 0
Vcc
Wire Wire Line
	5200 3350 5075 3350
Connection ~ 5200 3350
$EndSCHEMATC
