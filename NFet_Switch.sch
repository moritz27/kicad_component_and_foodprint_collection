EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 20 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5E15BF0D
P 6140 4210
AR Path="/61189DB7/5E15BF0D" Ref="Q?"  Part="1" 
AR Path="/6119E070/5E15BF0D" Ref="Q?"  Part="1" 
F 0 "Q?" H 6346 4256 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 6346 4165 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6340 4310 50  0001 C CNN
F 3 "~" H 6140 4210 50  0001 C CNN
	1    6140 4210
	1    0    0    -1  
$EndComp
Text Label 5840 4210 2    50   ~ 0
Gate
Text Label 6240 3910 0    50   ~ 0
Drain
Text Label 6240 4495 0    50   ~ 0
Source
Wire Wire Line
	6240 4410 6240 4495
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5E164601
P 6030 3275
AR Path="/61189DB7/5E164601" Ref="J?"  Part="1" 
AR Path="/6119E070/5E164601" Ref="J?"  Part="1" 
F 0 "J?" V 5994 3087 50  0000 R CNN
F 1 "Conn_01x02" V 5903 3087 50  0001 R CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 6030 3275 50  0001 C CNN
F 3 "~" H 6030 3275 50  0001 C CNN
	1    6030 3275
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E166553
P 6240 4495
AR Path="/61189DB7/5E166553" Ref="#PWR?"  Part="1" 
AR Path="/6119E070/5E166553" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6240 4245 50  0001 C CNN
F 1 "GND" H 6245 4322 50  0000 C CNN
F 2 "" H 6240 4495 50  0001 C CNN
F 3 "" H 6240 4495 50  0001 C CNN
	1    6240 4495
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E166C43
P 5495 4210
AR Path="/61189DB7/5E166C43" Ref="R?"  Part="1" 
AR Path="/6119E070/5E166C43" Ref="R?"  Part="1" 
F 0 "R?" V 5288 4210 50  0000 C CNN
F 1 "1k" V 5379 4210 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5425 4210 50  0001 C CNN
F 3 "~" H 5495 4210 50  0001 C CNN
	1    5495 4210
	0    1    1    0   
$EndComp
Wire Wire Line
	5645 4210 5940 4210
Wire Wire Line
	5345 4210 5225 4210
$Comp
L Device:LED D?
U 1 1 5E1CD1BB
P 5835 5000
AR Path="/61189DB7/5E1CD1BB" Ref="D?"  Part="1" 
AR Path="/6119E070/5E1CD1BB" Ref="D?"  Part="1" 
F 0 "D?" H 5828 4745 50  0000 C CNN
F 1 "LED" H 5828 4836 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5835 5000 50  0001 C CNN
F 3 "~" H 5835 5000 50  0001 C CNN
	1    5835 5000
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5E1CD1C1
P 5490 5000
AR Path="/61189DB7/5E1CD1C1" Ref="R?"  Part="1" 
AR Path="/6119E070/5E1CD1C1" Ref="R?"  Part="1" 
F 0 "R?" V 5283 5000 50  0000 C CNN
F 1 "1k" V 5374 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5420 5000 50  0001 C CNN
F 3 "~" H 5490 5000 50  0001 C CNN
	1    5490 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	5640 5000 5685 5000
$Comp
L power:GND #PWR?
U 1 1 5E1CD1C8
P 6080 5065
AR Path="/61189DB7/5E1CD1C8" Ref="#PWR?"  Part="1" 
AR Path="/6119E070/5E1CD1C8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6080 4815 50  0001 C CNN
F 1 "GND" H 6085 4892 50  0000 C CNN
F 2 "" H 6080 5065 50  0001 C CNN
F 3 "" H 6080 5065 50  0001 C CNN
	1    6080 5065
	1    0    0    -1  
$EndComp
Wire Wire Line
	5985 5000 6080 5000
Wire Wire Line
	6080 5000 6080 5065
Wire Wire Line
	5225 5000 5340 5000
Connection ~ 5225 4210
Wire Wire Line
	5225 4210 5190 4210
Wire Wire Line
	5225 4210 5225 5000
Wire Wire Line
	5885 3665 5915 3665
Wire Wire Line
	6240 3665 6215 3665
Wire Wire Line
	6240 3665 6240 4010
Wire Wire Line
	5885 3665 5885 3475
Wire Wire Line
	5885 3475 6030 3475
Wire Wire Line
	6130 3475 6240 3475
Wire Wire Line
	6240 3475 6240 3665
Connection ~ 6240 3665
$Comp
L Device:D_Schottky D?
U 1 1 60A8684D
P 6065 3665
AR Path="/61189DB7/60A8684D" Ref="D?"  Part="1" 
AR Path="/6119E070/60A8684D" Ref="D?"  Part="1" 
F 0 "D?" H 6430 3665 50  0000 C CNN
F 1 "D_Schottky" H 6065 3790 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 6065 3665 50  0001 C CNN
F 3 "~" H 6065 3665 50  0001 C CNN
	1    6065 3665
	1    0    0    -1  
$EndComp
Text HLabel 5190 4210 0    50   Input ~ 0
On
Text HLabel 5830 3665 0    50   Input ~ 0
Vcc
Wire Wire Line
	5830 3665 5885 3665
Connection ~ 5885 3665
$EndSCHEMATC
