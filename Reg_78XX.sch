EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 21 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Linear:KA78M05_TO252 U?
U 1 1 5DE734CF
P 5590 3540
F 0 "U?" H 5590 3782 50  0000 C CNN
F 1 "KA78M09_TO252" H 5590 3691 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 5590 3765 50  0001 C CIN
F 3 "http://www.fairchildsemi.com/ds/LM/LM78M05.pdf" H 5590 3490 50  0001 C CNN
	1    5590 3540
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5DE7D130
P 5230 3820
AR Path="/5DA62A26/5DE7D130" Ref="C?"  Part="1" 
AR Path="/5DE868BC/5DE7D130" Ref="C?"  Part="1" 
AR Path="/6119532D/5DE7D130" Ref="C?"  Part="1" 
F 0 "C?" H 5345 3866 50  0000 L CNN
F 1 "C 100nF" H 5345 3775 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5268 3670 50  0001 C CNN
F 3 "~" H 5230 3820 50  0001 C CNN
	1    5230 3820
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5DE7E19B
P 5990 3790
AR Path="/5DA62A26/5DE7E19B" Ref="C?"  Part="1" 
AR Path="/5DE868BC/5DE7E19B" Ref="C?"  Part="1" 
AR Path="/6119532D/5DE7E19B" Ref="C?"  Part="1" 
F 0 "C?" H 6105 3836 50  0000 L CNN
F 1 "C 100nF" H 6105 3745 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6028 3640 50  0001 C CNN
F 3 "~" H 5990 3790 50  0001 C CNN
	1    5990 3790
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5DE7E95E
P 6590 3790
AR Path="/5DA62A26/5DE7E95E" Ref="C?"  Part="1" 
AR Path="/5DE868BC/5DE7E95E" Ref="C?"  Part="1" 
AR Path="/6119532D/5DE7E95E" Ref="C?"  Part="1" 
F 0 "C?" H 6705 3836 50  0000 L CNN
F 1 "C 10uF" H 6705 3745 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 6628 3640 50  0001 C CNN
F 3 "~" H 6590 3790 50  0001 C CNN
	1    6590 3790
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3540 5230 3540
Wire Wire Line
	5230 3670 5230 3540
Connection ~ 5230 3540
Wire Wire Line
	5230 3540 5290 3540
Wire Wire Line
	5230 4080 5590 4080
Wire Wire Line
	5590 4080 5590 3840
Wire Wire Line
	5590 4080 5990 4080
Wire Wire Line
	5990 4080 5990 3940
Connection ~ 5590 4080
Wire Wire Line
	5890 3540 5990 3540
Wire Wire Line
	5990 3540 5990 3640
Wire Wire Line
	5990 3540 6590 3540
Wire Wire Line
	6590 3540 6590 3640
Connection ~ 5990 3540
Wire Wire Line
	5990 4080 6590 4080
Wire Wire Line
	6590 4080 6590 3940
Connection ~ 5990 4080
Wire Wire Line
	5230 3970 5230 4080
$Comp
L power:GND #PWR?
U 1 1 5DED015B
P 5230 4120
AR Path="/5DA62A26/5DED015B" Ref="#PWR?"  Part="1" 
AR Path="/5DE868BC/5DED015B" Ref="#PWR?"  Part="1" 
AR Path="/6119532D/5DED015B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5230 3870 50  0001 C CNN
F 1 "GND" H 5140 4070 50  0000 R CNN
F 2 "" H 5230 4120 50  0001 C CNN
F 3 "" H 5230 4120 50  0001 C CNN
	1    5230 4120
	1    0    0    -1  
$EndComp
Wire Wire Line
	5230 4120 5230 4080
Connection ~ 5230 4080
Text HLabel 4550 3540 0    50   Input ~ 0
Vin
Text HLabel 6880 3540 2    50   Output ~ 0
Vout
Wire Wire Line
	6590 3540 6880 3540
Connection ~ 6590 3540
$EndSCHEMATC
