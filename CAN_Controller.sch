EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 23 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Interface_CAN_LIN:TJA1051T-3 U?
U 1 1 611DDCF4
P 7090 3340
F 0 "U?" H 6870 3730 50  0000 C CNN
F 1 "TJA1051T-3" H 7370 3720 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7090 2840 50  0001 C CIN
F 3 "http://www.nxp.com/documents/data_sheet/TJA1051.pdf" H 7090 3340 50  0001 C CNN
	1    7090 3340
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal Y?
U 1 1 611E0DBE
P 3880 3940
AR Path="/60EDB96D/611E0DBE" Ref="Y?"  Part="1" 
AR Path="/611DCAD3/611E0DBE" Ref="Y?"  Part="1" 
F 0 "Y?" V 3834 4071 50  0000 L CNN
F 1 "8MHz" V 3925 4071 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_HC49-SD" H 3880 3940 50  0001 C CNN
F 3 "~" H 3880 3940 50  0001 C CNN
	1    3880 3940
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 611E0DC4
P 3280 3630
AR Path="/5FDB8EF8/611E0DC4" Ref="C?"  Part="1" 
AR Path="/6003A36D/611E0DC4" Ref="C?"  Part="1" 
AR Path="/60EDB96D/611E0DC4" Ref="C?"  Part="1" 
AR Path="/611DCAD3/611E0DC4" Ref="C?"  Part="1" 
F 0 "C?" V 3410 3680 50  0000 L CNN
F 1 "20pF" V 3410 3390 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3318 3480 50  0001 C CNN
F 3 "~" H 3280 3630 50  0001 C CNN
	1    3280 3630
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 611E0DCA
P 3280 4240
AR Path="/5FDB8EF8/611E0DCA" Ref="C?"  Part="1" 
AR Path="/6003A36D/611E0DCA" Ref="C?"  Part="1" 
AR Path="/60EDB96D/611E0DCA" Ref="C?"  Part="1" 
AR Path="/611DCAD3/611E0DCA" Ref="C?"  Part="1" 
F 0 "C?" V 3130 4330 50  0000 L CNN
F 1 "20pF" V 3130 4020 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3318 4090 50  0001 C CNN
F 3 "~" H 3280 4240 50  0001 C CNN
	1    3280 4240
	0    1    1    0   
$EndComp
Wire Wire Line
	3430 3630 3880 3630
Wire Wire Line
	3880 3630 3880 3790
Wire Wire Line
	3430 4240 3880 4240
Wire Wire Line
	3880 4240 3880 4090
Wire Wire Line
	4320 3840 4320 3630
Wire Wire Line
	4320 4240 4320 3940
$Comp
L power:GND #PWR?
U 1 1 611E15EA
P 2650 4610
F 0 "#PWR?" H 2650 4360 50  0001 C CNN
F 1 "GND" H 2655 4437 50  0000 C CNN
F 2 "" H 2650 4610 50  0001 C CNN
F 3 "" H 2650 4610 50  0001 C CNN
	1    2650 4610
	1    0    0    -1  
$EndComp
Wire Wire Line
	4320 3940 4610 3940
Wire Wire Line
	4610 3840 4320 3840
$Comp
L Interface_CAN_LIN:MCP2515-xSO U?
U 1 1 611DCDB3
P 5210 3640
F 0 "U?" H 4910 4430 50  0000 C CNN
F 1 "MCP2515-xSO" H 5570 4430 50  0000 C CNN
F 2 "Package_SO:SOIC-18W_7.5x11.6mm_P1.27mm" H 5210 2740 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21801e.pdf" H 5310 2840 50  0001 C CNN
	1    5210 3640
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 4610 2650 4240
Wire Wire Line
	2650 4240 3130 4240
Wire Wire Line
	2650 4240 2650 3630
Wire Wire Line
	2650 3630 3130 3630
Connection ~ 2650 4240
Text HLabel 4420 3040 0    50   Input ~ 0
MOSI
Text HLabel 4420 3140 0    50   Output ~ 0
MISO
Text HLabel 4420 3240 0    50   Input ~ 0
CS
Text HLabel 4420 3340 0    50   Input ~ 0
CLK
Wire Wire Line
	4420 3040 4610 3040
Wire Wire Line
	4420 3140 4610 3140
Wire Wire Line
	4420 3240 4610 3240
Wire Wire Line
	4420 3340 4610 3340
Wire Wire Line
	5810 3140 6590 3140
Wire Wire Line
	6590 3240 6180 3240
Wire Wire Line
	6180 3240 6180 3040
Wire Wire Line
	6180 3040 5810 3040
$Comp
L power:GND #PWR?
U 1 1 611EC7B4
P 5210 4500
F 0 "#PWR?" H 5210 4250 50  0001 C CNN
F 1 "GND" H 5215 4327 50  0000 C CNN
F 2 "" H 5210 4500 50  0001 C CNN
F 3 "" H 5210 4500 50  0001 C CNN
	1    5210 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5210 4500 5210 4440
$Comp
L power:GND #PWR?
U 1 1 611ED1E9
P 7090 4190
F 0 "#PWR?" H 7090 3940 50  0001 C CNN
F 1 "GND" H 7095 4017 50  0000 C CNN
F 2 "" H 7090 4190 50  0001 C CNN
F 3 "" H 7090 4190 50  0001 C CNN
	1    7090 4190
	1    0    0    -1  
$EndComp
Text HLabel 7090 2490 1    50   Input ~ 0
5V
Wire Wire Line
	7090 2940 7090 2650
Text HLabel 5210 2500 1    50   Input ~ 0
3V3
Wire Wire Line
	5210 2840 5210 2650
$Comp
L Device:C C?
U 1 1 611EF8D4
P 5440 1740
AR Path="/5FDB8EF8/611EF8D4" Ref="C?"  Part="1" 
AR Path="/6003A36D/611EF8D4" Ref="C?"  Part="1" 
AR Path="/60EDB96D/611EF8D4" Ref="C?"  Part="1" 
AR Path="/611DCAD3/611EF8D4" Ref="C?"  Part="1" 
F 0 "C?" H 5555 1786 50  0000 L CNN
F 1 "10uF" H 5555 1695 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 5478 1590 50  0001 C CNN
F 3 "~" H 5440 1740 50  0001 C CNN
	1    5440 1740
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 611EF8DA
P 6970 1730
AR Path="/5FDB8EF8/611EF8DA" Ref="C?"  Part="1" 
AR Path="/6003A36D/611EF8DA" Ref="C?"  Part="1" 
AR Path="/60EDB96D/611EF8DA" Ref="C?"  Part="1" 
AR Path="/611DCAD3/611EF8DA" Ref="C?"  Part="1" 
F 0 "C?" H 7085 1776 50  0000 L CNN
F 1 "10uF" H 7085 1685 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 7008 1580 50  0001 C CNN
F 3 "~" H 6970 1730 50  0001 C CNN
	1    6970 1730
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 611EF8E0
P 7410 1730
AR Path="/5FDB8EF8/611EF8E0" Ref="C?"  Part="1" 
AR Path="/6003A36D/611EF8E0" Ref="C?"  Part="1" 
AR Path="/60EDB96D/611EF8E0" Ref="C?"  Part="1" 
AR Path="/611DCAD3/611EF8E0" Ref="C?"  Part="1" 
F 0 "C?" H 7525 1776 50  0000 L CNN
F 1 "100nF" H 7525 1685 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7448 1580 50  0001 C CNN
F 3 "~" H 7410 1730 50  0001 C CNN
	1    7410 1730
	1    0    0    -1  
$EndComp
Wire Wire Line
	6970 1880 7410 1880
Wire Wire Line
	6970 1580 6970 1440
Wire Wire Line
	6970 1440 7410 1440
Wire Wire Line
	5440 1590 5440 1450
Wire Wire Line
	4950 1890 5440 1890
Wire Wire Line
	4950 1590 4950 1450
$Comp
L Device:C C?
U 1 1 611EF8F7
P 4950 1740
AR Path="/5FDB8EF8/611EF8F7" Ref="C?"  Part="1" 
AR Path="/6003A36D/611EF8F7" Ref="C?"  Part="1" 
AR Path="/60EDB96D/611EF8F7" Ref="C?"  Part="1" 
AR Path="/611DCAD3/611EF8F7" Ref="C?"  Part="1" 
F 0 "C?" H 5065 1786 50  0000 L CNN
F 1 "100nF" H 5065 1695 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4988 1590 50  0001 C CNN
F 3 "~" H 4950 1740 50  0001 C CNN
	1    4950 1740
	1    0    0    -1  
$EndComp
Wire Wire Line
	7410 1580 7410 1440
Text HLabel 6970 1340 1    50   Input ~ 0
5V
$Comp
L power:GND #PWR?
U 1 1 611F638D
P 4950 2010
F 0 "#PWR?" H 4950 1760 50  0001 C CNN
F 1 "GND" H 4955 1837 50  0000 C CNN
F 2 "" H 4950 2010 50  0001 C CNN
F 3 "" H 4950 2010 50  0001 C CNN
	1    4950 2010
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 2010 4950 1890
Text HLabel 4950 1360 1    50   Input ~ 0
3V3
Wire Wire Line
	4950 1450 5440 1450
Wire Wire Line
	4950 1450 4950 1360
Connection ~ 4950 1450
Wire Wire Line
	6970 1340 6970 1440
Connection ~ 6970 1440
$Comp
L power:GND #PWR?
U 1 1 611F8F88
P 6970 2000
F 0 "#PWR?" H 6970 1750 50  0001 C CNN
F 1 "GND" H 6975 1827 50  0000 C CNN
F 2 "" H 6970 2000 50  0001 C CNN
F 3 "" H 6970 2000 50  0001 C CNN
	1    6970 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6970 2000 6970 1880
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 611FF6D3
P 8400 3300
F 0 "J?" H 8480 3292 50  0000 L CNN
F 1 "Conn_01x02-Connector_Generic" H 8480 3201 50  0000 L CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 8400 3300 50  0001 C CNN
F 3 "" H 8400 3300 50  0001 C CNN
	1    8400 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7590 3240 7800 3240
Wire Wire Line
	8030 3240 8030 3300
Wire Wire Line
	8030 3300 8200 3300
Wire Wire Line
	7590 3440 8030 3440
Wire Wire Line
	8030 3440 8030 3400
Wire Wire Line
	8030 3400 8200 3400
Text HLabel 5880 3640 2    50   Output ~ 0
Int
Text HLabel 5890 4240 2    50   Input ~ 0
Reset
Wire Wire Line
	5810 4240 5890 4240
Wire Wire Line
	5810 3640 5880 3640
Wire Wire Line
	5810 3740 5880 3740
Wire Wire Line
	5810 3840 5880 3840
Wire Wire Line
	5810 3940 5880 3940
Wire Wire Line
	5810 4040 5880 4040
Wire Wire Line
	5810 4140 5880 4140
NoConn ~ 5880 4140
NoConn ~ 5880 4040
NoConn ~ 5880 3940
NoConn ~ 5880 3840
NoConn ~ 5880 3740
$Comp
L Device:R R?
U 1 1 61206A6D
P 7800 3950
F 0 "R?" H 7870 3996 50  0000 L CNN
F 1 "120" H 7870 3905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7730 3950 50  0001 C CNN
F 3 "~" H 7800 3950 50  0001 C CNN
	1    7800 3950
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP?
U 1 1 61207836
P 7800 4350
F 0 "JP?" V 7800 4262 50  0000 R CNN
F 1 "SolderJumper_2_Open" V 7755 4262 50  0001 R CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 7800 4350 50  0001 C CNN
F 3 "~" H 7800 4350 50  0001 C CNN
	1    7800 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7800 3800 7800 3240
Connection ~ 7800 3240
Wire Wire Line
	7800 3240 8030 3240
Wire Wire Line
	7800 4100 7800 4200
Wire Wire Line
	7800 4500 7800 4620
Wire Wire Line
	7800 4620 8030 4620
Wire Wire Line
	8030 4620 8030 3440
Connection ~ 8030 3440
$Comp
L Jumper:SolderJumper_3_Open JP?
U 1 1 6120BB95
P 6410 2650
F 0 "JP?" H 6410 2763 50  0000 C CNN
F 1 "SolderJumper_3_Open" H 6410 2764 50  0001 C CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6410 2650 50  0001 C CNN
F 3 "~" H 6410 2650 50  0001 C CNN
	1    6410 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6210 2650 5210 2650
Connection ~ 5210 2650
Wire Wire Line
	5210 2650 5210 2500
Wire Wire Line
	6610 2650 7090 2650
Connection ~ 7090 2650
Wire Wire Line
	7090 2650 7090 2490
Wire Wire Line
	6410 2800 6410 3440
Wire Wire Line
	6410 3440 6590 3440
Text HLabel 6470 3540 0    50   Input ~ 0
Silent
Wire Wire Line
	6470 3540 6560 3540
$Comp
L Device:R R?
U 1 1 61215217
P 6560 3790
F 0 "R?" H 6630 3836 50  0000 L CNN
F 1 "10k" H 6630 3745 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6490 3790 50  0001 C CNN
F 3 "~" H 6560 3790 50  0001 C CNN
	1    6560 3790
	1    0    0    -1  
$EndComp
Wire Wire Line
	6560 3640 6560 3540
Connection ~ 6560 3540
Wire Wire Line
	6560 3540 6590 3540
Wire Wire Line
	6560 3940 6560 4050
Wire Wire Line
	6560 4050 7090 4050
Wire Wire Line
	7090 3740 7090 4050
Connection ~ 7090 4050
Wire Wire Line
	7090 4050 7090 4190
Wire Wire Line
	3880 3630 4320 3630
Connection ~ 3880 3630
Wire Wire Line
	3880 4240 4320 4240
Connection ~ 3880 4240
Wire Wire Line
	4610 4040 4540 4040
NoConn ~ 4540 4040
$EndSCHEMATC
