EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4680 3420 4890 3420
Connection ~ 4890 3420
$Comp
L power:GND #PWR?
U 1 1 603602D2
P 5280 4140
AR Path="/60457E44/603602D2" Ref="#PWR?"  Part="1" 
AR Path="/60457E44/60359E52/603602D2" Ref="#PWR?"  Part="1" 
AR Path="/60ED35A4/603602D2" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 5280 3890 50  0001 C CNN
F 1 "GND" H 5285 3967 50  0000 C CNN
F 2 "" H 5280 4140 50  0001 C CNN
F 3 "" H 5280 4140 50  0001 C CNN
	1    5280 4140
	1    0    0    -1  
$EndComp
Wire Wire Line
	5280 4140 5280 4070
$Comp
L Device:L L?
U 1 1 603602D9
P 5300 2850
AR Path="/60457E44/603602D9" Ref="L?"  Part="1" 
AR Path="/60457E44/60359E52/603602D9" Ref="L?"  Part="1" 
AR Path="/60ED35A4/603602D9" Ref="L1"  Part="1" 
F 0 "L1" V 5400 2910 50  0000 C CNN
F 1 "22uH" V 5400 2780 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 5300 2850 50  0001 C CNN
F 3 "~" H 5300 2850 50  0001 C CNN
	1    5300 2850
	0    -1   -1   0   
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 603602DF
P 6080 2850
AR Path="/6003A36D/603602DF" Ref="D?"  Part="1" 
AR Path="/60457E44/603602DF" Ref="D?"  Part="1" 
AR Path="/60457E44/60359E52/603602DF" Ref="D?"  Part="1" 
AR Path="/60ED35A4/603602DF" Ref="D1"  Part="1" 
F 0 "D1" H 6080 2740 50  0000 C CNN
F 1 "SS34" H 5930 2730 50  0001 C CNN
F 2 "Diode_SMD:D_SMA" H 6080 2850 50  0001 C CNN
F 3 "~" H 6080 2850 50  0001 C CNN
	1    6080 2850
	-1   0    0    1   
$EndComp
Wire Wire Line
	5580 3420 5700 3420
Wire Wire Line
	5700 3420 5700 2850
Wire Wire Line
	5700 2850 5450 2850
Wire Wire Line
	5700 2850 5930 2850
Connection ~ 5700 2850
Wire Wire Line
	4890 3420 4890 2850
Wire Wire Line
	4890 2850 5150 2850
$Comp
L Device:C C?
U 1 1 603602EC
P 4890 3840
AR Path="/5FD94276/603602EC" Ref="C?"  Part="1" 
AR Path="/5FD932E8/603602EC" Ref="C?"  Part="1" 
AR Path="/603602EC" Ref="C?"  Part="1" 
AR Path="/60457E44/603602EC" Ref="C?"  Part="1" 
AR Path="/60457E44/60359E52/603602EC" Ref="C?"  Part="1" 
AR Path="/60ED35A4/603602EC" Ref="C1"  Part="1" 
F 0 "C1" H 4630 3800 50  0000 L CNN
F 1 "22u" H 4630 3890 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4928 3690 50  0001 C CNN
F 3 "~" H 4890 3840 50  0001 C CNN
	1    4890 3840
	1    0    0    1   
$EndComp
Wire Wire Line
	4890 3990 4890 4070
Wire Wire Line
	4890 4070 5280 4070
Connection ~ 5280 4070
Wire Wire Line
	5280 4070 5280 3820
$Comp
L Device:C C?
U 1 1 603602F8
P 6780 3470
AR Path="/5FD94276/603602F8" Ref="C?"  Part="1" 
AR Path="/5FD932E8/603602F8" Ref="C?"  Part="1" 
AR Path="/603602F8" Ref="C?"  Part="1" 
AR Path="/60457E44/603602F8" Ref="C?"  Part="1" 
AR Path="/60457E44/60359E52/603602F8" Ref="C?"  Part="1" 
AR Path="/60ED35A4/603602F8" Ref="C2"  Part="1" 
F 0 "C2" H 6950 3430 50  0000 L CNN
F 1 "22u" H 6930 3550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6818 3320 50  0001 C CNN
F 3 "~" H 6780 3470 50  0001 C CNN
	1    6780 3470
	1    0    0    1   
$EndComp
Wire Wire Line
	6780 4070 6780 3620
Wire Wire Line
	6780 3320 6780 2850
Wire Wire Line
	6230 2850 6350 2850
Wire Wire Line
	5280 4070 6350 4070
$Comp
L Device:R R?
U 1 1 60360302
P 6350 3220
AR Path="/5FD94276/60360302" Ref="R?"  Part="1" 
AR Path="/60360302" Ref="R?"  Part="1" 
AR Path="/60457E44/60360302" Ref="R?"  Part="1" 
AR Path="/60457E44/60359E52/60360302" Ref="R?"  Part="1" 
AR Path="/60ED35A4/60360302" Ref="R1"  Part="1" 
F 0 "R1" H 6430 3190 50  0000 L CNN
F 1 "33k" H 6430 3270 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6280 3220 50  0001 C CNN
F 3 "~" H 6350 3220 50  0001 C CNN
	1    6350 3220
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 60360308
P 6350 3830
AR Path="/5FD94276/60360308" Ref="R?"  Part="1" 
AR Path="/60360308" Ref="R?"  Part="1" 
AR Path="/60457E44/60360308" Ref="R?"  Part="1" 
AR Path="/60457E44/60359E52/60360308" Ref="R?"  Part="1" 
AR Path="/60ED35A4/60360308" Ref="R2"  Part="1" 
F 0 "R2" H 6430 3800 50  0000 L CNN
F 1 "4k7" H 6430 3880 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6280 3830 50  0001 C CNN
F 3 "~" H 6350 3830 50  0001 C CNN
	1    6350 3830
	1    0    0    1   
$EndComp
Wire Wire Line
	6350 3070 6350 2850
Connection ~ 6350 2850
Wire Wire Line
	6350 2850 6780 2850
Wire Wire Line
	6350 3980 6350 4070
Connection ~ 6350 4070
Wire Wire Line
	6350 4070 6780 4070
Wire Wire Line
	5580 3620 6350 3620
Wire Wire Line
	6350 3370 6350 3620
Connection ~ 6350 3620
Wire Wire Line
	6350 3620 6350 3680
Text Notes 5670 4390 0    50   ~ 0
Vout = Vref x (1+ R1/R2); Vref = 0,6V\nR1 = R2 x (5V/0,6 -1)  \nR1 = R2 x 7,3
Text Notes 5140 2460 0    101  ~ 20
Step Up Converter
Text Notes 6190 3250 0    50   ~ 0
R1
Text Notes 6190 3860 0    50   ~ 0
R2
Text Label 5900 3620 0    50   ~ 0
Vref
Text HLabel 4680 3420 0    50   Input ~ 0
Vin
Text HLabel 6880 2850 2    50   Output ~ 0
Vout
Wire Wire Line
	6780 2850 6880 2850
Connection ~ 6780 2850
$Comp
L my_libary:MT3608(SOT23-6) U?
U 1 1 603602C6
P 5280 3520
AR Path="/60457E44/603602C6" Ref="U?"  Part="1" 
AR Path="/60457E44/60359E52/603602C6" Ref="U?"  Part="1" 
AR Path="/60ED35A4/603602C6" Ref="U1"  Part="1" 
F 0 "U1" H 5150 3790 50  0000 C CNN
F 1 "MT3608" H 5380 3780 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 5330 3270 50  0001 L CIN
F 3 "https://www.olimex.com/Products/Breadboarding/BB-PWR-3608/resources/MT3608.pdf" H 5030 3970 50  0001 C CNN
	1    5280 3520
	1    0    0    -1  
$EndComp
Wire Wire Line
	4890 3420 4890 3520
Wire Wire Line
	5080 3520 4890 3520
Connection ~ 4890 3520
Wire Wire Line
	4890 3520 4890 3690
Wire Wire Line
	4890 3420 5080 3420
Wire Wire Line
	5280 3820 5080 3820
Wire Wire Line
	5080 3820 5080 3620
$EndSCHEMATC
