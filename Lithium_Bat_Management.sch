EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Battery_Cell BT?
U 1 1 61171F50
P 6440 4390
AR Path="/60457E44/61171F50" Ref="BT?"  Part="1" 
AR Path="/60457E44/60362EF5/61171F50" Ref="BT?"  Part="1" 
AR Path="/60EDB86E/61171F50" Ref="BT?"  Part="1" 
F 0 "BT?" H 6558 4486 50  0000 L CNN
F 1 "18650_Li_Ion_Battery_Cell" H 6558 4395 50  0001 L CNN
F 2 "Battery:BatteryHolder_MPD_BH-18650-PC2" V 6440 4450 50  0001 C CNN
F 3 "~" V 6440 4450 50  0001 C CNN
	1    6440 4390
	1    0    0    -1  
$EndComp
$Comp
L my_libary:DW01 U?
U 1 1 6036F9DA
P 4620 4090
AR Path="/60457E44/6036F9DA" Ref="U?"  Part="1" 
AR Path="/60457E44/60362EF5/6036F9DA" Ref="U?"  Part="1" 
AR Path="/60EDB86E/6036F9DA" Ref="U?"  Part="1" 
F 0 "U?" H 5120 4550 50  0000 C CNN
F 1 "DW01" H 5330 4550 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 5070 4540 50  0001 C CNN
F 3 "" H 5070 4540 50  0001 C CNN
	1    4620 4090
	1    0    0    -1  
$EndComp
$Comp
L my_libary:FS8205 U?
U 1 1 61171F53
P 5740 4860
AR Path="/60457E44/61171F53" Ref="U?"  Part="1" 
AR Path="/60457E44/60362EF5/61171F53" Ref="U?"  Part="1" 
AR Path="/60EDB86E/61171F53" Ref="U?"  Part="1" 
F 0 "U?" H 6150 5270 50  0000 C CNN
F 1 "FS8205" H 6360 5270 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 5740 4860 50  0001 C CNN
F 3 "" H 5740 4860 50  0001 C CNN
	1    5740 4860
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 61171F54
P 6060 4340
AR Path="/5FD94276/61171F54" Ref="C?"  Part="1" 
AR Path="/5FD932E8/61171F54" Ref="C?"  Part="1" 
AR Path="/61171F54" Ref="C?"  Part="1" 
AR Path="/60457E44/61171F54" Ref="C?"  Part="1" 
AR Path="/60457E44/60362EF5/61171F54" Ref="C?"  Part="1" 
AR Path="/60EDB86E/61171F54" Ref="C?"  Part="1" 
F 0 "C?" H 5830 4300 50  0000 L CNN
F 1 "10u" H 5800 4390 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6098 4190 50  0001 C CNN
F 3 "~" H 6060 4340 50  0001 C CNN
	1    6060 4340
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 61171F55
P 6880 3460
AR Path="/5FD94276/61171F55" Ref="R?"  Part="1" 
AR Path="/61171F55" Ref="R?"  Part="1" 
AR Path="/60457E44/61171F55" Ref="R?"  Part="1" 
AR Path="/60457E44/60362EF5/61171F55" Ref="R?"  Part="1" 
AR Path="/60EDB86E/61171F55" Ref="R?"  Part="1" 
F 0 "R?" H 6960 3430 50  0000 L CNN
F 1 "1k" H 6960 3510 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6810 3460 50  0001 C CNN
F 3 "~" H 6880 3460 50  0001 C CNN
	1    6880 3460
	1    0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 6036F9F2
P 6880 3830
AR Path="/60457E44/6036F9F2" Ref="D?"  Part="1" 
AR Path="/60457E44/60362EF5/6036F9F2" Ref="D?"  Part="1" 
AR Path="/60EDB86E/6036F9F2" Ref="D?"  Part="1" 
F 0 "D?" V 6827 3910 50  0000 L CNN
F 1 "LED" V 6918 3910 50  0000 L CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6880 3830 50  0001 C CNN
F 3 "~" H 6880 3830 50  0001 C CNN
	1    6880 3830
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 61171F57
P 7290 3460
AR Path="/5FD94276/61171F57" Ref="R?"  Part="1" 
AR Path="/61171F57" Ref="R?"  Part="1" 
AR Path="/60457E44/61171F57" Ref="R?"  Part="1" 
AR Path="/60457E44/60362EF5/61171F57" Ref="R?"  Part="1" 
AR Path="/60EDB86E/61171F57" Ref="R?"  Part="1" 
F 0 "R?" H 7370 3430 50  0000 L CNN
F 1 "1k" H 7370 3510 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7220 3460 50  0001 C CNN
F 3 "~" H 7290 3460 50  0001 C CNN
	1    7290 3460
	1    0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 61171F58
P 7290 3830
AR Path="/60457E44/61171F58" Ref="D?"  Part="1" 
AR Path="/60457E44/60362EF5/61171F58" Ref="D?"  Part="1" 
AR Path="/60EDB86E/61171F58" Ref="D?"  Part="1" 
F 0 "D?" V 7237 3910 50  0000 L CNN
F 1 "LED" V 7328 3910 50  0000 L CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 7290 3830 50  0001 C CNN
F 3 "~" H 7290 3830 50  0001 C CNN
	1    7290 3830
	0    1    1    0   
$EndComp
Wire Wire Line
	6250 3240 6880 3240
Wire Wire Line
	6880 3240 6880 3310
Wire Wire Line
	7290 3140 6250 3140
Wire Wire Line
	7290 3140 7290 3310
Wire Wire Line
	7290 3680 7290 3660
Wire Wire Line
	6250 3040 6430 3040
Wire Wire Line
	6880 3980 6880 4060
Wire Wire Line
	7290 4060 7290 3980
$Comp
L Device:C C?
U 1 1 61171F5B
P 4560 3270
AR Path="/5FD94276/61171F5B" Ref="C?"  Part="1" 
AR Path="/5FD932E8/61171F5B" Ref="C?"  Part="1" 
AR Path="/61171F5B" Ref="C?"  Part="1" 
AR Path="/60457E44/61171F5B" Ref="C?"  Part="1" 
AR Path="/60457E44/60362EF5/61171F5B" Ref="C?"  Part="1" 
AR Path="/60EDB86E/61171F5B" Ref="C?"  Part="1" 
F 0 "C?" H 4330 3230 50  0000 L CNN
F 1 "100nF" H 4190 3320 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4598 3120 50  0001 C CNN
F 3 "~" H 4560 3270 50  0001 C CNN
	1    4560 3270
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6036FA29
P 4380 3120
AR Path="/60457E44/6036FA29" Ref="#PWR?"  Part="1" 
AR Path="/60457E44/60362EF5/6036FA29" Ref="#PWR?"  Part="1" 
AR Path="/6036FA29" Ref="#PWR?"  Part="1" 
AR Path="/60EDB86E/6036FA29" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4380 2870 50  0001 C CNN
F 1 "GND" V 4385 2992 50  0000 R CNN
F 2 "" H 4380 3120 50  0001 C CNN
F 3 "" H 4380 3120 50  0001 C CNN
	1    4380 3120
	0    1    1    0   
$EndComp
Wire Wire Line
	5180 3420 5180 3340
Wire Wire Line
	5180 3340 5500 3340
Wire Wire Line
	4950 3040 5500 3040
Wire Wire Line
	6250 3340 6440 3340
Wire Wire Line
	6440 3340 6440 3890
$Comp
L Device:R R?
U 1 1 6036FA47
P 6000 3890
AR Path="/5FD94276/6036FA47" Ref="R?"  Part="1" 
AR Path="/6036FA47" Ref="R?"  Part="1" 
AR Path="/60457E44/6036FA47" Ref="R?"  Part="1" 
AR Path="/60457E44/60362EF5/6036FA47" Ref="R?"  Part="1" 
AR Path="/60EDB86E/6036FA47" Ref="R?"  Part="1" 
F 0 "R?" V 6090 3740 50  0000 L CNN
F 1 "100" V 6090 3910 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5930 3890 50  0001 C CNN
F 3 "~" H 6000 3890 50  0001 C CNN
	1    6000 3890
	0    -1   1    0   
$EndComp
Wire Wire Line
	6150 3890 6440 3890
Connection ~ 6440 3890
Wire Wire Line
	6440 3890 6440 4080
$Comp
L Device:C C?
U 1 1 6036FA50
P 5770 3690
AR Path="/5FD94276/6036FA50" Ref="C?"  Part="1" 
AR Path="/5FD932E8/6036FA50" Ref="C?"  Part="1" 
AR Path="/6036FA50" Ref="C?"  Part="1" 
AR Path="/60457E44/6036FA50" Ref="C?"  Part="1" 
AR Path="/60457E44/60362EF5/6036FA50" Ref="C?"  Part="1" 
AR Path="/60EDB86E/6036FA50" Ref="C?"  Part="1" 
F 0 "C?" H 5500 3650 50  0000 L CNN
F 1 "100nF" H 5420 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5808 3540 50  0001 C CNN
F 3 "~" H 5770 3690 50  0001 C CNN
	1    5770 3690
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5770 3840 5770 3890
Wire Wire Line
	5520 3890 5770 3890
Connection ~ 5770 3890
Wire Wire Line
	5770 3890 5850 3890
Wire Wire Line
	5520 3790 5610 3790
Wire Wire Line
	5610 3790 5610 3660
Wire Wire Line
	5610 3540 5770 3540
Text Label 6530 4570 0    50   ~ 0
B-
Text Label 6520 4080 0    50   ~ 0
B+
Wire Wire Line
	4940 4560 4770 4560
Wire Wire Line
	4770 4560 4770 3990
Wire Wire Line
	4770 3990 4920 3990
Wire Wire Line
	4940 4760 4390 4760
Wire Wire Line
	4390 4760 4390 3790
Wire Wire Line
	4390 3790 4920 3790
Wire Wire Line
	5490 4760 6440 4760
Wire Wire Line
	6060 4080 6440 4080
Wire Wire Line
	6060 4080 6060 4190
Connection ~ 6440 4080
Wire Wire Line
	6440 4080 6440 4190
Wire Wire Line
	6440 4080 6520 4080
Wire Wire Line
	6530 4570 6440 4570
Wire Wire Line
	6440 4490 6440 4570
Wire Wire Line
	6440 4570 6440 4760
Connection ~ 6440 4570
Text Label 5690 4430 0    50   ~ 0
OUT-
Wire Wire Line
	5490 4560 5690 4560
Wire Wire Line
	5690 4560 6060 4560
Wire Wire Line
	6060 4560 6060 4490
Connection ~ 5690 4560
NoConn ~ 5520 3990
$Comp
L Device:R R?
U 1 1 61171F61
P 4600 4240
AR Path="/5FD94276/61171F61" Ref="R?"  Part="1" 
AR Path="/61171F61" Ref="R?"  Part="1" 
AR Path="/60457E44/61171F61" Ref="R?"  Part="1" 
AR Path="/60457E44/60362EF5/61171F61" Ref="R?"  Part="1" 
AR Path="/60EDB86E/61171F61" Ref="R?"  Part="1" 
F 0 "R?" V 4650 3970 50  0000 L CNN
F 1 "1k2" V 4650 4360 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4530 4240 50  0001 C CNN
F 3 "~" H 4600 4240 50  0001 C CNN
	1    4600 4240
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 4090 4600 3890
Wire Wire Line
	4600 3890 4920 3890
Text Label 4600 4490 3    50   ~ 0
OUT-
Wire Wire Line
	4600 4490 4600 4390
NoConn ~ 4940 4660
NoConn ~ 5490 4660
$Comp
L power:GND #PWR?
U 1 1 61171F62
P 5690 4330
AR Path="/60457E44/61171F62" Ref="#PWR?"  Part="1" 
AR Path="/60457E44/60362EF5/61171F62" Ref="#PWR?"  Part="1" 
AR Path="/61171F62" Ref="#PWR?"  Part="1" 
AR Path="/60EDB86E/61171F62" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5690 4080 50  0001 C CNN
F 1 "GND" H 5695 4157 50  0000 C CNN
F 2 "" H 5690 4330 50  0001 C CNN
F 3 "" H 5690 4330 50  0001 C CNN
	1    5690 4330
	-1   0    0    1   
$EndComp
Wire Wire Line
	5690 4330 5690 4560
Text Notes 4840 2270 0    101  ~ 20
Lithium Battery Management
$Comp
L power:+5V #PWR?
U 1 1 61171F5A
P 7090 4060
AR Path="/60457E44/61171F5A" Ref="#PWR?"  Part="1" 
AR Path="/60457E44/60362EF5/61171F5A" Ref="#PWR?"  Part="1" 
AR Path="/60EDB86E/61171F5A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7090 3910 50  0001 C CNN
F 1 "+5V" H 7105 4233 50  0000 C CNN
F 2 "" H 7090 4060 50  0001 C CNN
F 3 "" H 7090 4060 50  0001 C CNN
	1    7090 4060
	-1   0    0    1   
$EndComp
Wire Wire Line
	6430 2860 6430 3040
$Comp
L power:+5V #PWR?
U 1 1 61171F5C
P 4390 3420
AR Path="/60457E44/61171F5C" Ref="#PWR?"  Part="1" 
AR Path="/60457E44/60362EF5/61171F5C" Ref="#PWR?"  Part="1" 
AR Path="/60EDB86E/61171F5C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4390 3270 50  0001 C CNN
F 1 "+5V" V 4405 3548 50  0000 L CNN
F 2 "" H 4390 3420 50  0001 C CNN
F 3 "" H 4390 3420 50  0001 C CNN
	1    4390 3420
	0    -1   -1   0   
$EndComp
Text HLabel 8770 3700 2    50   Output ~ 0
Out+
Text Label 8660 3700 2    50   ~ 0
B+
Wire Wire Line
	8660 3700 8770 3700
$Comp
L power:+5V #PWR?
U 1 1 61171F59
P 6430 2860
AR Path="/60457E44/61171F59" Ref="#PWR?"  Part="1" 
AR Path="/60457E44/60362EF5/61171F59" Ref="#PWR?"  Part="1" 
AR Path="/60EDB86E/61171F59" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6430 2710 50  0001 C CNN
F 1 "+5V" H 6445 3033 50  0000 C CNN
F 2 "" H 6430 2860 50  0001 C CNN
F 3 "" H 6430 2860 50  0001 C CNN
	1    6430 2860
	1    0    0    -1  
$EndComp
$Comp
L my_libary:TP4056 U?
U 1 1 6036F9D4
P 5200 3440
AR Path="/60457E44/6036F9D4" Ref="U?"  Part="1" 
AR Path="/60457E44/60362EF5/6036F9D4" Ref="U?"  Part="1" 
AR Path="/60EDB86E/6036F9D4" Ref="U?"  Part="1" 
F 0 "U?" H 5875 4105 50  0000 C CNN
F 1 "TP4056" H 5875 4014 50  0000 C CNN
F 2 "Package_SO:SOP-8_3.76x4.96mm_P1.27mm" H 6050 3990 50  0001 C CNN
F 3 "" H 6050 3990 50  0001 C CNN
	1    5200 3440
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 3120 4950 3040
Wire Wire Line
	5500 3240 4950 3240
Connection ~ 4950 3120
Wire Wire Line
	4950 3240 4950 3120
Wire Wire Line
	5500 3120 5500 3140
Wire Wire Line
	5410 3120 5500 3120
Wire Wire Line
	4950 3120 5110 3120
$Comp
L Device:R R?
U 1 1 61171F5E
P 5260 3120
AR Path="/5FD94276/61171F5E" Ref="R?"  Part="1" 
AR Path="/61171F5E" Ref="R?"  Part="1" 
AR Path="/60457E44/61171F5E" Ref="R?"  Part="1" 
AR Path="/60457E44/60362EF5/61171F5E" Ref="R?"  Part="1" 
AR Path="/60EDB86E/61171F5E" Ref="R?"  Part="1" 
F 0 "R?" V 5320 2870 50  0000 L CNN
F 1 "1k2" V 5310 3240 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5190 3120 50  0001 C CNN
F 3 "~" H 5260 3120 50  0001 C CNN
	1    5260 3120
	0    -1   1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 61171F7D
P 7880 3730
F 0 "J?" H 7960 3772 50  0000 L CNN
F 1 "Conn_01x03" H 7960 3681 50  0000 L CNN
F 2 "Connector_JST:JST_PH_B3B-PH-K_1x03_P2.00mm_Vertical" H 7880 3730 50  0001 C CNN
F 3 "~" H 7880 3730 50  0001 C CNN
	1    7880 3730
	1    0    0    -1  
$EndComp
Wire Wire Line
	7290 3660 7510 3660
Wire Wire Line
	7510 3660 7510 3730
Wire Wire Line
	7510 3730 7680 3730
Connection ~ 7290 3660
Wire Wire Line
	7290 3660 7290 3610
Wire Wire Line
	7290 4060 7680 4060
Wire Wire Line
	7680 4060 7680 3830
Connection ~ 7290 4060
Wire Wire Line
	6880 3610 6880 3630
Wire Wire Line
	7680 3630 6880 3630
Connection ~ 6880 3630
Wire Wire Line
	6880 3630 6880 3680
Wire Wire Line
	5510 3660 5610 3660
Connection ~ 5610 3660
Wire Wire Line
	5610 3660 5610 3540
Text Label 5510 3540 0    50   ~ 0
B-
Wire Wire Line
	5510 3540 5510 3660
Wire Wire Line
	4380 3120 4950 3120
Wire Wire Line
	4390 3420 5180 3420
Wire Wire Line
	6880 4060 7290 4060
$EndSCHEMATC
