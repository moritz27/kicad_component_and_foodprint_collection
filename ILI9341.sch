EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 14 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 5300 3390 2    50   ~ 0
TFT_CS
$Comp
L Device:Q_PMOS_GSD Q?
U 1 1 60F9D317
P 4050 3730
AR Path="/60EDB96D/60F9D317" Ref="Q?"  Part="1" 
AR Path="/60F9B3A9/60F9D317" Ref="Q?"  Part="1" 
F 0 "Q?" H 4255 3730 50  0000 L CNN
F 1 "AO3401A" H 4255 3775 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4250 3655 50  0001 L CIN
F 3 "http://www.aosmd.com/pdfs/datasheet/AO3401A.pdf" H 4050 3730 50  0001 L CNN
	1    4050 3730
	1    0    0    1   
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 60F9D31D
P 4150 3400
AR Path="/60EDB96D/60F9D31D" Ref="#PWR?"  Part="1" 
AR Path="/60F9B3A9/60F9D31D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4150 3250 50  0001 C CNN
F 1 "VCC" H 4165 3573 50  0000 C CNN
F 2 "" H 4150 3400 50  0001 C CNN
F 3 "" H 4150 3400 50  0001 C CNN
	1    4150 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3530 4150 3460
Text Label 3700 3730 2    50   ~ 0
TFT_LED
Wire Wire Line
	4150 3930 4150 4090
Wire Wire Line
	3700 3730 3780 3730
$Comp
L Device:R R?
U 1 1 60F9D327
P 3780 3510
AR Path="/5FDB8EF8/60F9D327" Ref="R?"  Part="1" 
AR Path="/6003A36D/60F9D327" Ref="R?"  Part="1" 
AR Path="/60EDB96D/60F9D327" Ref="R?"  Part="1" 
AR Path="/60F9B3A9/60F9D327" Ref="R?"  Part="1" 
F 0 "R?" H 3880 3460 50  0000 C CNN
F 1 "47k" H 3890 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3710 3510 50  0001 C CNN
F 3 "~" H 3780 3510 50  0001 C CNN
	1    3780 3510
	-1   0    0    1   
$EndComp
Wire Wire Line
	3780 3660 3780 3730
Connection ~ 3780 3730
Wire Wire Line
	3780 3730 3850 3730
Wire Wire Line
	3780 3360 3780 3310
Wire Wire Line
	3780 3310 3960 3310
Wire Wire Line
	3960 3310 3960 3460
Wire Wire Line
	3960 3460 4150 3460
Connection ~ 4150 3460
Wire Wire Line
	4150 3460 4150 3400
$Comp
L Jumper:SolderJumper_3_Open JP?
U 1 1 60F9D336
P 4770 3790
AR Path="/5FD932E8/60F9D336" Ref="JP?"  Part="1" 
AR Path="/6003A36D/60F9D336" Ref="JP?"  Part="1" 
AR Path="/60EDB96D/60F9D336" Ref="JP?"  Part="1" 
AR Path="/60F9B3A9/60F9D336" Ref="JP?"  Part="1" 
F 0 "JP?" V 4770 3857 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 4815 3857 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm_NumberLabels" H 4770 3790 50  0001 C CNN
F 3 "~" H 4770 3790 50  0001 C CNN
	1    4770 3790
	0    -1   1    0   
$EndComp
Wire Wire Line
	4150 3460 4770 3460
Wire Wire Line
	4150 4090 4770 4090
Wire Wire Line
	4770 4090 4770 3990
Wire Wire Line
	4770 3460 4770 3590
$Comp
L Driver_Display:CR2013-MI2120 U?
U 1 1 60F9D340
P 6150 3590
AR Path="/60EDB96D/60F9D340" Ref="U?"  Part="1" 
AR Path="/60F9B3A9/60F9D340" Ref="U?"  Part="1" 
F 0 "U?" H 5650 4160 50  0000 C CNN
F 1 "CR2013-MI2120" H 6610 4160 50  0000 C CNN
F 2 "Display:CR2013-MI2120" H 6150 2890 50  0001 C CNN
F 3 "http://pan.baidu.com/s/11Y990" H 5500 4090 50  0001 C CNN
	1    6150 3590
	-1   0    0    -1  
$EndComp
Text Label 5300 3490 2    50   ~ 0
MOSI
Text Label 5300 3890 2    50   ~ 0
TFT_RST
Text Label 5300 3690 2    50   ~ 0
CLK
Text Label 5300 3590 2    50   ~ 0
MISO
NoConn ~ 6950 3590
$Comp
L power:GND #PWR?
U 1 1 60F9D34B
P 6150 4240
AR Path="/5FDB8EF8/60F9D34B" Ref="#PWR?"  Part="1" 
AR Path="/6003A36D/60F9D34B" Ref="#PWR?"  Part="1" 
AR Path="/60F9D34B" Ref="#PWR?"  Part="1" 
AR Path="/60EDB96D/60F9D34B" Ref="#PWR?"  Part="1" 
AR Path="/60F9B3A9/60F9D34B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6150 3990 50  0001 C CNN
F 1 "GND" H 6155 4067 50  0000 C CNN
F 2 "" H 6150 4240 50  0001 C CNN
F 3 "" H 6150 4240 50  0001 C CNN
	1    6150 4240
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 4240 6150 4190
Wire Wire Line
	5300 3890 5350 3890
Wire Wire Line
	5300 3690 5350 3690
Wire Wire Line
	5300 3590 5350 3590
Wire Wire Line
	5300 3490 5350 3490
Wire Wire Line
	5300 3390 5350 3390
Text Label 5300 3290 2    50   ~ 0
TFT_DC
Wire Wire Line
	5350 3290 5300 3290
Text Notes 4430 2970 0    101  ~ 20
Display
Wire Wire Line
	4920 3790 5350 3790
Wire Wire Line
	6150 2990 6150 2940
Text Label 7040 3290 0    50   ~ 0
MOSI
Text Label 7040 3390 0    50   ~ 0
MISO
Text Label 7040 3490 0    50   ~ 0
CLK
Wire Wire Line
	6950 3190 7040 3190
Wire Wire Line
	6950 3290 7040 3290
Wire Wire Line
	6950 3390 7040 3390
Wire Wire Line
	6950 3490 7040 3490
$Comp
L power:VCC #PWR?
U 1 1 60F9D364
P 6150 2940
AR Path="/60EDB96D/60F9D364" Ref="#PWR?"  Part="1" 
AR Path="/60F9B3A9/60F9D364" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6150 2790 50  0001 C CNN
F 1 "VCC" V 6165 3067 50  0000 L CNN
F 2 "" H 6150 2940 50  0001 C CNN
F 3 "" H 6150 2940 50  0001 C CNN
	1    6150 2940
	1    0    0    -1  
$EndComp
Text Label 7040 3190 0    50   ~ 0
SD_CS
$EndSCHEMATC
