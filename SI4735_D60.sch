EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF_AM_FM:Si4735-D60-GU U?
U 1 1 5FD93FF6
P 5900 3700
F 0 "U?" H 6300 4450 50  0000 C CNN
F 1 "Si4735-D60-GU" H 5300 4450 50  0000 C CNN
F 2 "Package_SO:SSOP-24_3.9x8.7mm_P0.635mm" H 6150 3000 50  0001 L CNN
F 3 "http://www.silabs.com/Support%20Documents/TechnicalDocs/Si4730-31-34-35-D60.pdf" H 5950 2700 50  0001 C CNN
	1    5900 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 61171F11
P 7360 3900
AR Path="/5FD94276/61171F11" Ref="C?"  Part="1" 
AR Path="/5FD932E8/61171F11" Ref="C?"  Part="1" 
AR Path="/60EDB7B0/61171F11" Ref="C?"  Part="1" 
F 0 "C?" V 7510 3950 50  0000 L CNN
F 1 "10nF" V 7510 3750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7398 3750 50  0001 C CNN
F 3 "~" H 7360 3900 50  0001 C CNN
	1    7360 3900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 61171F12
P 7370 4250
AR Path="/5FD94276/61171F12" Ref="C?"  Part="1" 
AR Path="/5FD932E8/61171F12" Ref="C?"  Part="1" 
AR Path="/60EDB7B0/61171F12" Ref="C?"  Part="1" 
F 0 "C?" V 7520 4300 50  0000 L CNN
F 1 "1nF" V 7520 4100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7408 4100 50  0001 C CNN
F 3 "~" H 7370 4250 50  0001 C CNN
	1    7370 4250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6650 4100 6500 4100
Wire Wire Line
	6500 4000 6850 4000
Wire Wire Line
	6850 4000 6850 4250
$Comp
L Device:C C?
U 1 1 5FD92C67
P 6800 2990
AR Path="/5FD94276/5FD92C67" Ref="C?"  Part="1" 
AR Path="/5FD932E8/5FD92C67" Ref="C?"  Part="1" 
AR Path="/60EDB7B0/5FD92C67" Ref="C?"  Part="1" 
F 0 "C?" H 6850 3090 50  0000 L CNN
F 1 "22nF" H 6860 2880 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6838 2840 50  0001 C CNN
F 3 "~" H 6800 2990 50  0001 C CNN
	1    6800 2990
	1    0    0    -1  
$EndComp
Text Label 5800 2750 2    50   ~ 0
VD
Text Label 6000 2750 2    50   ~ 0
VA
Text HLabel 5150 3200 0    50   Input ~ 0
RST
Text HLabel 5150 3500 0    50   BiDi ~ 0
SDIO
Text HLabel 5150 3600 0    50   Input ~ 0
SCLK
Wire Wire Line
	5150 3500 5300 3500
Wire Wire Line
	5150 3600 5300 3600
Wire Wire Line
	5150 3200 5300 3200
$Comp
L Jumper:SolderJumper_3_Open JP?
U 1 1 5FD94581
P 4100 2880
F 0 "JP?" V 4100 2947 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 4145 2947 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm_NumberLabels" H 4100 2880 50  0001 C CNN
F 3 "~" H 4100 2880 50  0001 C CNN
	1    4100 2880
	0    -1   1    0   
$EndComp
Wire Wire Line
	5800 2750 5800 2900
Wire Wire Line
	5800 4500 5800 4750
Wire Wire Line
	5800 4750 6000 4750
Wire Wire Line
	6000 4750 6000 4500
Connection ~ 6000 4750
$Comp
L power:GND #PWR?
U 1 1 5FD96F27
P 6000 5000
F 0 "#PWR?" H 6000 4750 50  0001 C CNN
F 1 "GND" H 6005 4827 50  0000 C CNN
F 2 "" H 6000 5000 50  0001 C CNN
F 3 "" H 6000 5000 50  0001 C CNN
	1    6000 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 4750 6000 5000
$Comp
L Device:Crystal Y?
U 1 1 61171F1B
P 4590 3950
F 0 "Y?" V 4410 3830 50  0000 L CNN
F 1 "Crystal" V 4970 3820 50  0000 L CNN
F 2 "Crystal:Crystal_DS26_D2.0mm_L6.0mm_Vertical" H 4590 3950 50  0001 C CNN
F 3 "~" H 4590 3950 50  0001 C CNN
	1    4590 3950
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5FD9906C
P 4160 4250
AR Path="/5FD94276/5FD9906C" Ref="C?"  Part="1" 
AR Path="/5FD932E8/5FD9906C" Ref="C?"  Part="1" 
AR Path="/60EDB7B0/5FD9906C" Ref="C?"  Part="1" 
F 0 "C?" V 4310 4300 50  0000 L CNN
F 1 "22pF" V 4310 4100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4198 4100 50  0001 C CNN
F 3 "~" H 4160 4250 50  0001 C CNN
	1    4160 4250
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 61171F1F
P 4140 3700
AR Path="/5FD94276/61171F1F" Ref="C?"  Part="1" 
AR Path="/5FD932E8/61171F1F" Ref="C?"  Part="1" 
AR Path="/60EDB7B0/61171F1F" Ref="C?"  Part="1" 
F 0 "C?" V 4290 3750 50  0000 L CNN
F 1 "22pF" V 4290 3550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4178 3550 50  0001 C CNN
F 3 "~" H 4140 3700 50  0001 C CNN
	1    4140 3700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4010 4250 3980 4250
Wire Wire Line
	3980 3700 3990 3700
Wire Wire Line
	4920 3900 5300 3900
Wire Wire Line
	4920 4250 4920 3900
Wire Wire Line
	6800 3200 6500 3200
Text HLabel 6650 3500 2    50   Output ~ 0
ROUT
Text HLabel 6650 3600 2    50   Output ~ 0
LOUT
NoConn ~ 6500 3400
NoConn ~ 6500 3700
Text Label 4100 2580 2    50   ~ 0
VD
Wire Wire Line
	4100 2680 4100 2580
$Comp
L power:GND #PWR?
U 1 1 5FDA6235
P 4100 3130
F 0 "#PWR?" H 4100 2880 50  0001 C CNN
F 1 "GND" H 4105 2957 50  0000 C CNN
F 2 "" H 4100 3130 50  0001 C CNN
F 3 "" H 4100 3130 50  0001 C CNN
	1    4100 3130
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 3130 4100 3080
Wire Wire Line
	6500 3500 6650 3500
Wire Wire Line
	6500 3600 6650 3600
Text HLabel 4100 2050 0    50   Input ~ 0
Vin
Wire Wire Line
	4500 2050 4500 2250
Wire Wire Line
	4300 2050 4500 2050
Wire Wire Line
	4100 2050 4300 2050
Connection ~ 4300 2050
Wire Wire Line
	4300 2050 4300 2250
Text Label 4300 2250 0    50   ~ 0
VA
Text Label 4500 2250 0    50   ~ 0
VD
$Comp
L power:GND #PWR?
U 1 1 5FDAF3F7
P 4800 2150
F 0 "#PWR?" H 4800 1900 50  0001 C CNN
F 1 "GND" H 4805 1977 50  0000 C CNN
F 2 "" H 4800 2150 50  0001 C CNN
F 3 "" H 4800 2150 50  0001 C CNN
	1    4800 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FDAE85D
P 4650 2050
AR Path="/5FD94276/5FDAE85D" Ref="C?"  Part="1" 
AR Path="/5FD932E8/5FDAE85D" Ref="C?"  Part="1" 
AR Path="/60EDB7B0/5FDAE85D" Ref="C?"  Part="1" 
F 0 "C?" V 4800 2100 50  0000 L CNN
F 1 "100nF" V 4800 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4688 1900 50  0001 C CNN
F 3 "~" H 4650 2050 50  0001 C CNN
	1    4650 2050
	0    -1   -1   0   
$EndComp
Connection ~ 4500 2050
Wire Wire Line
	4800 2150 4800 2050
Wire Wire Line
	4450 2880 4250 2880
Wire Wire Line
	4290 3700 4590 3700
Wire Wire Line
	4590 3800 4590 3700
Wire Wire Line
	4590 4100 4590 4250
Wire Wire Line
	4310 4250 4590 4250
Wire Wire Line
	3980 3700 3980 4250
Wire Wire Line
	4450 2880 4450 3400
$Comp
L power:GND #PWR?
U 1 1 61171F33
P 3980 4360
F 0 "#PWR?" H 3980 4110 50  0001 C CNN
F 1 "GND" H 3985 4187 50  0000 C CNN
F 2 "" H 3980 4360 50  0001 C CNN
F 3 "" H 3980 4360 50  0001 C CNN
	1    3980 4360
	1    0    0    -1  
$EndComp
Wire Wire Line
	3980 4250 3980 4360
Connection ~ 3980 4250
Connection ~ 4590 3700
Connection ~ 4590 4250
Wire Wire Line
	4590 3700 5300 3700
Wire Wire Line
	4590 4250 4920 4250
Wire Wire Line
	6650 4100 6650 4750
Text HLabel 8060 4250 2    50   Input ~ 0
FM_Ant
Text HLabel 8060 3900 2    50   Input ~ 0
AM_Ant
Text HLabel 4970 4520 0    50   Output ~ 0
SI4735_INT
Text HLabel 4970 4620 0    50   Output ~ 0
SI4735_GPO1
Wire Wire Line
	4970 4520 5120 4520
Wire Wire Line
	5120 4520 5120 4000
Wire Wire Line
	5120 4000 5300 4000
Wire Wire Line
	4970 4620 5260 4620
Wire Wire Line
	5260 4620 5260 4100
Wire Wire Line
	5260 4100 5300 4100
$Comp
L Device:R R?
U 1 1 61171F7C
P 5020 3400
AR Path="/5FD94276/61171F7C" Ref="R?"  Part="1" 
AR Path="/61171F7C" Ref="R?"  Part="1" 
AR Path="/5FD932E8/61171F7C" Ref="R?"  Part="1" 
AR Path="/60EDB7B0/61171F7C" Ref="R?"  Part="1" 
F 0 "R?" V 5100 3440 50  0000 L CNN
F 1 "10k" V 5100 3270 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4950 3400 50  0001 C CNN
F 3 "~" H 5020 3400 50  0001 C CNN
	1    5020 3400
	0    1    -1   0   
$EndComp
Wire Wire Line
	5170 3400 5300 3400
Wire Wire Line
	4870 3400 4450 3400
Wire Wire Line
	6000 2750 6000 2770
Wire Wire Line
	6800 3200 6800 3140
Wire Wire Line
	6000 2770 6800 2770
Connection ~ 6000 2770
Wire Wire Line
	6000 2770 6000 2900
Wire Wire Line
	6000 4750 6650 4750
Wire Wire Line
	6800 2770 6800 2840
Wire Wire Line
	6500 3900 7210 3900
Wire Wire Line
	6850 4250 7220 4250
Wire Wire Line
	7510 3900 8060 3900
Wire Wire Line
	7520 4250 8060 4250
$EndSCHEMATC
