EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 5450 2610 0    101  ~ 20
Linear Regulator
Text HLabel 4840 3170 0    50   Input ~ 0
Vin
Text HLabel 7340 3170 2    50   Output ~ 0
Vout
Text Notes 5580 4860 0    50   ~ 0
Vout = Vset x (1+ R1/R2); Vref = 1,2V\nR1 = R2 x (2,7V/1,2 -1)\nR1 = R2 x 1,25\n15k = 12k x 1,25
Wire Wire Line
	6800 3560 6800 3520
Wire Wire Line
	6610 3560 6800 3560
Wire Wire Line
	6610 3270 6610 3560
Wire Wire Line
	6440 3170 6800 3170
Connection ~ 6800 3170
Wire Wire Line
	6800 3220 6800 3170
Connection ~ 6800 3560
Wire Wire Line
	6800 3600 6800 3560
Wire Wire Line
	6800 4010 7240 4010
Connection ~ 6800 4010
Wire Wire Line
	6800 3900 6800 4010
Wire Wire Line
	7240 4010 7240 3730
$Comp
L Device:R R?
U 1 1 60392079
P 6800 3750
AR Path="/5FD94276/60392079" Ref="R?"  Part="1" 
AR Path="/60392079" Ref="R?"  Part="1" 
AR Path="/60457E44/60392079" Ref="R?"  Part="1" 
AR Path="/60457E44/603908EF/60392079" Ref="R54"  Part="1" 
AR Path="/60ED52DB/60392079" Ref="R4"  Part="1" 
F 0 "R4" H 6880 3720 50  0000 L CNN
F 1 "12k" H 6880 3800 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6730 3750 50  0001 C CNN
F 3 "~" H 6800 3750 50  0001 C CNN
	1    6800 3750
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61171F64
P 6800 3370
AR Path="/5FD94276/61171F64" Ref="R?"  Part="1" 
AR Path="/61171F64" Ref="R?"  Part="1" 
AR Path="/60457E44/61171F64" Ref="R?"  Part="1" 
AR Path="/60457E44/603908EF/61171F64" Ref="R53"  Part="1" 
AR Path="/60ED52DB/61171F64" Ref="R3"  Part="1" 
F 0 "R3" H 6880 3340 50  0000 L CNN
F 1 "15k" H 6880 3420 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6730 3370 50  0001 C CNN
F 3 "~" H 6800 3370 50  0001 C CNN
	1    6800 3370
	1    0    0    1   
$EndComp
Wire Wire Line
	6040 3570 6040 4010
Wire Wire Line
	4910 3720 4910 4010
Wire Wire Line
	7340 3170 7240 3170
Wire Wire Line
	6800 3170 7240 3170
Connection ~ 7240 3170
Wire Wire Line
	7240 3170 7240 3430
Wire Wire Line
	4840 3170 4910 3170
Connection ~ 4910 3170
Wire Wire Line
	4910 3420 4910 3170
$Comp
L Device:C C?
U 1 1 61171F65
P 7240 3580
AR Path="/5FD94276/61171F65" Ref="C?"  Part="1" 
AR Path="/5FD932E8/61171F65" Ref="C?"  Part="1" 
AR Path="/61171F65" Ref="C?"  Part="1" 
AR Path="/60457E44/61171F65" Ref="C?"  Part="1" 
AR Path="/60457E44/603908EF/61171F65" Ref="C47"  Part="1" 
AR Path="/60ED52DB/61171F65" Ref="C4"  Part="1" 
F 0 "C4" H 6980 3540 50  0000 L CNN
F 1 "10u" H 6980 3630 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7278 3430 50  0001 C CNN
F 3 "~" H 7240 3580 50  0001 C CNN
	1    7240 3580
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60392095
P 4910 3570
AR Path="/5FD94276/60392095" Ref="C?"  Part="1" 
AR Path="/5FD932E8/60392095" Ref="C?"  Part="1" 
AR Path="/60392095" Ref="C?"  Part="1" 
AR Path="/60457E44/60392095" Ref="C?"  Part="1" 
AR Path="/60457E44/603908EF/60392095" Ref="C46"  Part="1" 
AR Path="/60ED52DB/60392095" Ref="C3"  Part="1" 
F 0 "C3" H 4650 3530 50  0000 L CNN
F 1 "10u" H 4650 3620 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4948 3420 50  0001 C CNN
F 3 "~" H 4910 3570 50  0001 C CNN
	1    4910 3570
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6610 3270 6440 3270
Wire Wire Line
	6040 4010 6800 4010
Wire Wire Line
	4910 4010 6040 4010
Connection ~ 6040 4010
Wire Wire Line
	6040 4190 6040 4010
$Comp
L Regulator_Linear:MAX604 U?
U 1 1 603920A6
P 6040 3270
AR Path="/60457E44/603920A6" Ref="U?"  Part="1" 
AR Path="/60457E44/603908EF/603920A6" Ref="U8"  Part="1" 
AR Path="/60ED52DB/603920A6" Ref="U2"  Part="1" 
F 0 "U2" H 6040 3612 50  0000 C CNN
F 1 "MAX604" H 6040 3521 50  0000 C CNN
F 2 "Package_SO:SO-8_3.9x4.9mm_P1.27mm" H 6040 3595 50  0001 C CIN
F 3 "http://datasheets.maximintegrated.com/en/ds/MAX603-MAX604.pdf" H 6040 3220 50  0001 C CNN
	1    6040 3270
	1    0    0    -1  
$EndComp
Text Notes 6640 3400 0    50   ~ 0
R1
Text Notes 6650 3780 0    50   ~ 0
R2
Text Notes 6400 3350 0    50   ~ 0
Vset
Wire Wire Line
	4910 3170 5640 3170
Text HLabel 5570 3270 0    50   Input ~ 0
OFF
Wire Wire Line
	5570 3270 5640 3270
$Comp
L power:GND #PWR02
U 1 1 60ED5CF9
P 6040 4190
F 0 "#PWR02" H 6040 3940 50  0001 C CNN
F 1 "GND" H 6045 4017 50  0000 C CNN
F 2 "" H 6040 4190 50  0001 C CNN
F 3 "" H 6040 4190 50  0001 C CNN
	1    6040 4190
	1    0    0    -1  
$EndComp
$EndSCHEMATC
