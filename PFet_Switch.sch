EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 22 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6030 4390 6030 4630
Wire Wire Line
	5580 4190 5660 4190
$Comp
L Device:R R?
U 1 1 6119EB65
P 5660 3970
AR Path="/5FDB8EF8/6119EB65" Ref="R?"  Part="1" 
AR Path="/6003A36D/6119EB65" Ref="R?"  Part="1" 
AR Path="/60EDB96D/6119EB65" Ref="R?"  Part="1" 
AR Path="/60F9B3A9/6119EB65" Ref="R?"  Part="1" 
AR Path="/6119EB65" Ref="R?"  Part="1" 
AR Path="/6119E070/6119EB65" Ref="R?"  Part="1" 
F 0 "R?" H 5760 3920 50  0000 C CNN
F 1 "47k" H 5770 4010 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5590 3970 50  0001 C CNN
F 3 "~" H 5660 3970 50  0001 C CNN
	1    5660 3970
	-1   0    0    1   
$EndComp
Wire Wire Line
	5660 4120 5660 4190
Connection ~ 5660 4190
Wire Wire Line
	5660 4190 5730 4190
Wire Wire Line
	5660 3820 5660 3770
Connection ~ 6030 3770
Wire Wire Line
	6030 3770 6030 3990
Wire Wire Line
	5660 3770 6030 3770
Text HLabel 5580 4190 0    50   Input ~ 0
On
Text HLabel 6030 4630 3    50   Output ~ 0
Vout
Text HLabel 6030 3620 1    50   Input ~ 0
Vcc
Wire Wire Line
	6030 3620 6030 3770
$Comp
L Device:Q_PMOS_GSD Q?
U 1 1 60EF0887
P 5930 4190
AR Path="/60EDB96D/60EF0887" Ref="Q?"  Part="1" 
AR Path="/60F9B3A9/60EF0887" Ref="Q?"  Part="1" 
AR Path="/6119E070/60EF0887" Ref="Q?"  Part="1" 
F 0 "Q?" H 6135 4190 50  0000 L CNN
F 1 "AO3401A" H 6135 4235 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6130 4115 50  0001 L CIN
F 3 "http://www.aosmd.com/pdfs/datasheet/AO3401A.pdf" H 5930 4190 50  0001 L CNN
	1    5930 4190
	1    0    0    1   
$EndComp
$EndSCHEMATC
