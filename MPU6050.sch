EESchema Schematic File Version 4
LIBS:test_project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 15 23
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Sensor_Motion:MPU-6050 U?
U 1 1 5DA62DA2
P 5300 3400
F 0 "U?" H 4950 2800 50  0000 C CNN
F 1 "MPU-6050" H 5600 2800 50  0000 C CNN
F 2 "Sensor_Motion:InvenSense_QFN-24_4x4mm_P0.5mm" H 5300 2600 50  0001 C CNN
F 3 "https://store.invensense.com/datasheets/invensense/MPU-6050_DataSheet_V3%204.pdf" H 5300 3250 50  0001 C CNN
	1    5300 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DA6434A
P 4200 2600
F 0 "R?" H 4030 2660 50  0000 L CNN
F 1 "4k7" H 4000 2550 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4130 2600 50  0001 C CNN
F 3 "~" H 4200 2600 50  0001 C CNN
	1    4200 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3200 4400 3200
Wire Wire Line
	4000 3100 4200 3100
Text HLabel 4000 3200 0    50   Input ~ 0
SCL
Text HLabel 4000 3100 0    50   BiDi ~ 0
SDA
Wire Wire Line
	4400 2350 4200 2350
Wire Wire Line
	4400 2350 5200 2350
Wire Wire Line
	5200 2350 5200 2700
Connection ~ 4400 2350
Wire Wire Line
	5200 2350 5400 2350
Wire Wire Line
	5400 2350 5400 2700
Connection ~ 5200 2350
Connection ~ 4200 3100
Wire Wire Line
	4200 3100 4600 3100
Connection ~ 4400 3200
Wire Wire Line
	4400 3200 4000 3200
Wire Wire Line
	4200 2750 4200 3100
Wire Wire Line
	4400 2600 4400 2350
Wire Wire Line
	4200 2450 4200 2350
Connection ~ 4200 2350
$Comp
L power:GND #PWR?
U 1 1 5DA67F8C
P 5300 4400
F 0 "#PWR?" H 5300 4150 50  0001 C CNN
F 1 "GND" H 5305 4227 50  0000 C CNN
F 2 "" H 5300 4400 50  0001 C CNN
F 3 "" H 5300 4400 50  0001 C CNN
	1    5300 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 4400 5300 4350
Wire Wire Line
	5300 4350 4450 4350
Wire Wire Line
	4450 4350 4450 3700
Wire Wire Line
	4450 3700 4600 3700
Connection ~ 5300 4350
Wire Wire Line
	5300 4350 5300 4100
Wire Wire Line
	4450 3700 4450 3600
Wire Wire Line
	4450 3600 4600 3600
Connection ~ 4450 3700
Wire Wire Line
	6650 3300 6650 3400
Connection ~ 6650 3400
Text HLabel 6100 3100 2    50   Input ~ 0
INT
Wire Wire Line
	6100 3100 6000 3100
Wire Wire Line
	6000 3300 6650 3300
Wire Wire Line
	6000 3400 6650 3400
Wire Wire Line
	5300 4350 6650 4350
$Comp
L Device:C C?
U 1 1 5DA6B6DC
P 6300 3600
F 0 "C?" V 6400 3750 50  0000 C CNN
F 1 "2200pF" V 6400 3450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6338 3450 50  0001 C CNN
F 3 "~" H 6300 3600 50  0001 C CNN
	1    6300 3600
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5DA6BF0C
P 6300 3900
F 0 "C?" V 6400 4050 50  0000 C CNN
F 1 "100nF" V 6400 3750 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6338 3750 50  0001 C CNN
F 3 "~" H 6300 3900 50  0001 C CNN
	1    6300 3900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6000 3600 6150 3600
Wire Wire Line
	6000 3700 6000 3900
Wire Wire Line
	6000 3900 6150 3900
Wire Wire Line
	6450 3600 6650 3600
Connection ~ 6650 3600
Wire Wire Line
	6650 3600 6650 3400
Wire Wire Line
	6450 3900 6650 3900
Wire Wire Line
	6650 3600 6650 3900
Connection ~ 6650 3900
Wire Wire Line
	6650 3900 6650 4350
$Comp
L Device:C C?
U 1 1 5DA6D7F7
P 6650 2650
F 0 "C?" H 6800 2750 50  0000 C CNN
F 1 "100nF" H 6800 2550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6688 2500 50  0001 C CNN
F 3 "~" H 6650 2650 50  0001 C CNN
	1    6650 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 2500 6650 2350
Wire Wire Line
	6650 2350 5400 2350
Connection ~ 5400 2350
Wire Wire Line
	6650 2800 6650 3300
Connection ~ 6650 3300
Wire Wire Line
	4400 2900 4400 3200
$Comp
L Device:R R?
U 1 1 5DA64794
P 4400 2750
F 0 "R?" H 4470 2796 50  0000 L CNN
F 1 "4k7" H 4470 2705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4330 2750 50  0001 C CNN
F 3 "~" H 4400 2750 50  0001 C CNN
	1    4400 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3300 4450 3300
Wire Wire Line
	4450 3300 4450 3600
Connection ~ 4450 3600
Text HLabel 4000 2200 0    50   Input ~ 0
Vcc
Wire Wire Line
	4200 2200 4000 2200
Wire Wire Line
	4200 2200 4200 2350
$EndSCHEMATC
